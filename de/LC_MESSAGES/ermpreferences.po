# Compendium of de.
msgid ""
msgstr ""
"Project-Id-Version: Koha Manual 22.11\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-06-15 01:13+0000\n"
"PO-Revision-Date: 2024-12-19 01:00+0000\n"
"Last-Translator: Lea Satzinger <lea.satzinger@posteo.de>\n"
"Language-Team: German <https://translate.koha-community.org/projects/koha-manual/ermpreferences/de/>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.6.2\n"
"X-Pootle-Path: /de/manual22.11/ermpreferences.po\n"
"X-Pootle-Revision: 1\n"
"X-POOTLE-MTIME: 1693999866.708281\n"

#: ../../source/ermpreferences.rst:6
msgid "E-Resource management"
msgstr "Electronic Resource Management"

#: ../../source/ermpreferences.rst:8
msgid "The E-Resource management tab in Koha will give you settings to enable and configure the :ref:`E-Resource management (ERM) module <erm-label>`."
msgstr "Die Registerkarte „E-Ressourcenmanagement“ in Koha bietet Einstellungen zur Aktivierung und Konfiguration des :ref:`E-Resource management (ERM) module <erm-label>`."

#: ../../source/ermpreferences.rst:11
msgid "Get there: More > Administration > System Preferences > E-Resource management"
msgstr "Pfad in Koha: Mehr > Administration > Systemparameter > ERM"

#: ../../source/ermpreferences.rst:16
msgid "Interface"
msgstr "Oberfläche"

#: ../../source/ermpreferences.rst:21
msgid "ERMModule"
msgstr "ERMModule"

#: ../../source/ermpreferences.rst:23
msgid "Asks: \\_\\_\\_ the e-resource management module"
msgstr "Fragt: \\_\\_\\_ das Modul E-Ressourcenmanagement"

#: ../../source/ermpreferences.rst:25 ../../source/ermpreferences.rst:88
msgid "Values:"
msgstr "Werte:"

#: ../../source/ermpreferences.rst:27
msgid "Disable"
msgstr "Deaktiviere"

#: ../../source/ermpreferences.rst:29
msgid "Enable"
msgstr "Aktivieren"

#: ../../source/ermpreferences.rst:31
msgid "Default: Disable"
msgstr "Voreinstellung: Deaktiviere"

#: ../../source/ermpreferences.rst:33 ../../source/ermpreferences.rst:52
#: ../../source/ermpreferences.rst:70 ../../source/ermpreferences.rst:98
msgid "Description:"
msgstr "Beschreibung:"

#: ../../source/ermpreferences.rst:35
msgid "This preference controls whether or not staff can use the :ref:`E-Resource management (ERM) module <erm-label>`."
msgstr "Dieser Systemparameter kontrolliert, ob Bibliothekspersonal das :ref:`E-Resource management (ERM) module <erm-label>` nutzen können."

#: ../../source/ermpreferences.rst:37
msgid "This is the main switch for the E-Resource management functionality."
msgstr "Dies ist der \"Hauptschalter\" für die ErM-Funktionalitäten."

#: ../../source/ermpreferences.rst:41
msgid "To use the module, staff also need to have the :ref:`erm permission <permission-erm-label>` (or the :ref:`superlibrarian permission <permission-superlibrarian-label>`)."
msgstr "Um das Modul zu benutzen, müssen die Mitarbeiter auch die :ref:`erm permission <permission-erm-label>` (oder die :ref:`superlibrarian permission <permission-superlibrarian-label>`) haben."

#: ../../source/ermpreferences.rst:48
msgid "ERMProviderEbscoApiKey"
msgstr "ERMProviderEbscoApiKey"

#: ../../source/ermpreferences.rst:50
msgid "Asks: API key for EBSCO HoldingsIQ \\_\\_\\_"
msgstr "Fragt: API-Schlüssel für EBSCO HoldingsIQ \\_\\_\\"

#: ../../source/ermpreferences.rst:55
msgid "knowledge base using their `HoldingsIQ <https://developer.ebsco.com/knowledge-services/holdingsiq>`__ tool. The library will need to ask EBSCO for both an API key and a Customer ID (see :ref:`ERMProviderEbscoCustomerID <erm-ermproviderebscocustomerid-label>` below). Once correctly configured the EBSCO knowledge base will be accessible via the :ref:`eHoldings <erm-eholdings-label>` menu option in E-Resource management."
msgstr "Knodlwedge Base unter Verwendung ihres Tools „HoldingsIQ <https://developer.ebsco.com/knowledge-services/holdingsiq>“. Die Bibliothek muss bei EBSCO sowohl einen API-Schlüssel als auch eine Kunden-ID anfordern (siehe :ref:`ERMProviderEbscoCustomerID <erm-ermproviderebscocustomerid-label>` unten). Nach korrekter Konfiguration ist die EBSCO-Wissensdatenbank über den Menüpunkt :ref:`eHoldings <erm-eholdings-label>` in der E-Ressourcenverwaltung zugänglich."

#: ../../source/ermpreferences.rst:59 ../../source/ermpreferences.rst:77
msgid "This preference allows libraries to integrate with the EBSCO global"
msgstr "Diese Einstellung ermöglicht Bibliotheken die Integration mit EBSCO global"

#: ../../source/ermpreferences.rst:61 ../../source/ermpreferences.rst:79
msgid "If this preference is left empty, you will be unable to connect the EBSCO knowledge base."
msgstr "Wenn Sie diese Einstellung leer lassen, können Sie keine Verbindung zur EBSCO Knowledge Base herstellen."

#: ../../source/ermpreferences.rst:66
msgid "ERMProviderEbscoCustomerID"
msgstr "ERMProviderEbscoCustomerID"

#: ../../source/ermpreferences.rst:68
msgid "Asks: Customer ID for EBSCO HoldingsIQ \\_\\_\\_"
msgstr "Fragt: Kunden-ID für EBSCO HoldingsIQ \\_\\_\\"

#: ../../source/ermpreferences.rst:73
msgid "knowledge base using their `HoldingsIQ <https://developer.ebsco.com/knowledge-services/holdingsiq>`__ tool. The library will need to ask EBSCO for both an API key and a Customer ID (see :ref:`ERMProviderEbscoApiKey <erm-ermproviderebscoapikey-label>` above). Once correctly configured the EBSCO knowledge base will be accessible via the :ref:`eHoldings <erm-eholdings-label>` menu option in E-Resource management."
msgstr "Knowledge Base unter Verwendung ihres Tools „HoldingsIQ <https://developer.ebsco.com/knowledge-services/holdingsiq>“. Die Bibliothek muss bei EBSCO sowohl einen API-Schlüssel als auch eine Kunden-ID anfordern (siehe :ref:`ERMProviderEbscoApiKey <erm-ermproviderebscoapikey-label>` oben). Nach korrekter Konfiguration ist die EBSCO-Wissensdatenbank über den Menüpunkt :ref:`eHoldings <erm-eholdings-label>` in der E-Ressourcenverwaltung zugänglich."

#: ../../source/ermpreferences.rst:84
msgid "ERMProviders"
msgstr "ERMProviders"

#: ../../source/ermpreferences.rst:86
msgid "Asks: Providers for the e-resource management module \\_\\_\\_"
msgstr "Fragt: Anbieter für das Modul E-Ressourcenmanagement \\_\\_\\"

#: ../../source/ermpreferences.rst:90
msgid "Select all"
msgstr "Alle auswählen"

#: ../../source/ermpreferences.rst:92
msgid "EBSCO"
msgstr "EBSCO"

#: ../../source/ermpreferences.rst:94
msgid "Local"
msgstr "Lokal"

#: ../../source/ermpreferences.rst:96
msgid "Default: Local"
msgstr "Voreinstellung: Lokal"

#: ../../source/ermpreferences.rst:101
msgid "This preference controls whether to show one or more sources of"
msgstr "Diese Einstellung steuert die Anzeige einer oder mehrerer Quellen von"

#: ../../source/ermpreferences.rst:101
msgid "eHoldings in the :ref:`eHoldings <erm-eholdings-label>` menu option."
msgstr "eHoldings in der Menüoption :ref:eHoldings <erm-eholdings-label>`."

#: ../../source/ermpreferences.rst:103
msgid "Selecting \"EBSCO\" allows integration with the EBSCO global knowledge base."
msgstr "Die Auswahl von „EBSCO“ ermöglicht die Integration mit der EBSCO global knowledge base."

#: ../../source/ermpreferences.rst:105
msgid "Selecting \"Local\" provides the ability to manually create eHoldings records for local sources."
msgstr "Wenn Sie „Lokal“ wählen, können Sie manuell eHoldings-Datensätze für lokale Quellen erstellen."
