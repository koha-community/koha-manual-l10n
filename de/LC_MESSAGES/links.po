# Compendium of de.
msgid ""
msgstr ""
"Project-Id-Version: Koha Manual 22.11\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-12 07:29-0300\n"
"PO-Revision-Date: 2023-07-27 18:00+0000\n"
"Last-Translator: Katrin Fischer <katrin.fischer@bsz-bw.de>\n"
"Language-Team: Koha Translation Team\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Pootle 2.8\n"
"X-Pootle-Path: /de/manual22.11/links.po\n"
"X-Pootle-Revision: 1\n"
"X-POOTLE-MTIME: 1690480827.970687\n"

#: ../../source/links.rst:6
msgid "Important links"
msgstr "Wichtige Links"

#: ../../source/links.rst:11
msgid "Koha related"
msgstr "In Bezug auf Koha"

#: ../../source/links.rst:13
msgid "Report Koha Bugs - http://bugs.koha-community.org"
msgstr "Fehler in Koha melden - http://bugs.koha-community.org"

#: ../../source/links.rst:15
msgid "Koha Versioning Control - http://git.koha-community.org/"
msgstr "Coderepository für Koha - http://git.koha-community.org/"

#: ../../source/links.rst:17
msgid "Database Structure - http://schema.koha-community.org"
msgstr "Datenbankstruktur - http://schema.koha-community.org"

#: ../../source/links.rst:19
msgid "Koha Community Statistics - http://hea.koha-community.org"
msgstr "Koha-Community-Statistiken - http://hea.koha-community.org"

#: ../../source/links.rst:21
msgid "Koha as a CMS - http://wiki.koha-community.org/wiki/Koha_as_a_CMS"
msgstr "Koha als CMS - http://wiki.koha-community.org/wiki/Koha_as_a_CMS"

#: ../../source/links.rst:23
msgid "Koha Bibliography - http://www.zotero.org/groups/koha"
msgstr "Koha-Bibliografie - http://www.zotero.org/groups/koha"

#: ../../source/links.rst:25
msgid "Koha Shared Links - http://groups.diigo.com/group/everything-koha"
msgstr "Koha-Linksammlung - http://groups.diigo.com/group/everything-koha"

#: ../../source/links.rst:30
msgid "Circulation related"
msgstr "In Bezug auf die Ausleihe"

#: ../../source/links.rst:32
msgid "Koha Desktop Offline Circulation: https://github.com/bywatersolutions/koha-offline-circulation/releases"
msgstr "Koha Offline-Verbuchungsprogramm: https://github.com/bywatersolutions/koha-offline-circulation/releases"

#: ../../source/links.rst:35
msgid "Koha Firefox Offline Circulation: https://addons.mozilla.org/en-US/firefox/addon/koct/"
msgstr "Koha Offline-Verbuchung für Firefox: https://addons.mozilla.org/en-US/firefox/addon/koct/"

#: ../../source/links.rst:41
msgid "Cataloging related"
msgstr "In Bezug auf die Katalogisierung"

#: ../../source/links.rst:43
msgid "Koha MARC Tutorials - http://www.pakban.net/brooke/"
msgstr "Koha MARC-Tutorials - http://www.pakban.net/brooke/"

#: ../../source/links.rst:45
msgid "IRSpy Open Z39.50 Server Search - http://irspy.indexdata.com/"
msgstr "IRSpy Suche nach freien Z39.50-Servern - http://irspy.indexdata.com/"

#: ../../source/links.rst:47
msgid "Z39.50 Server List - http://staff.library.mun.ca/staff/toolbox/z3950hosts.htm"
msgstr "Z39.50-Server-Liste - http://staff.library.mun.ca/staff/toolbox/z3950hosts.htm"

#: ../../source/links.rst:50
msgid "Open Koha Z39.50 Targets - http://wiki.koha-community.org/wiki/Koha_Open_Z39.50_Sources"
msgstr "Offene Koha-Z39.50-Quellen - http://wiki.koha-community.org/wiki/Koha_Open_Z39.50_Sources"

#: ../../source/links.rst:53
msgid "Library of Congress Authorities - http://authorities.loc.gov/"
msgstr "Library of Congress Normdaten - http://authorities.loc.gov/"

#: ../../source/links.rst:55
msgid "MARC Country Codes - http://www.loc.gov/marc/countries/"
msgstr "MARC-Ländercodes - http://www.loc.gov/marc/countries/"

#: ../../source/links.rst:57
msgid "Search the MARC Code List for Organizations - http://www.loc.gov/marc/organizations/org-search.php"
msgstr "Suche in der MARC-Codeliste für Organisationen - http://www.loc.gov/marc/organizations/org-search.php"

#: ../../source/links.rst:60
msgid "Search for Canadian MARC Codes - http://www.collectionscanada.gc.ca/illcandir-bin/illsear/l=0/c=1"
msgstr "Suche nach Kanadischen MARC-Codes - http://www.collectionscanada.gc.ca/illcandir-bin/illsear/l=0/c=1"

#: ../../source/links.rst:63
msgid "Z39.50 Bib-1 Attribute - http://www.loc.gov/z3950/agency/defns/bib1.html"
msgstr "Z39.50 Bib-1 Attribute - http://www.loc.gov/z3950/agency/defns/bib1.html"

#: ../../source/links.rst:69
msgid "Enhanced content related"
msgstr "In Bezug auf Kataloganreicherung"

#: ../../source/links.rst:71
msgid "Amazon Associates - `https://affiliate-program.amazon.com <https://affiliate-program.amazon.com/>`__"
msgstr "Amazon Associates - `https://affiliate-program.amazon.com <https://affiliate-program.amazon.com/>`__"

#: ../../source/links.rst:74
msgid "Amazon Web Services - http://aws.amazon.com"
msgstr "Amazon Web Services - http://aws.amazon.com"

#: ../../source/links.rst:76
msgid "WorldCat Affiliate Tools - http://www.worldcat.org/wcpa/do/AffiliateUserServices?method=initSelfRegister"
msgstr "WorldCat Affiliate Tools - http://www.worldcat.org/wcpa/do/AffiliateUserServices?method=initSelfRegister"

#: ../../source/links.rst:79
msgid "LibraryThing for Libraries - http://www.librarything.com/forlibraries"
msgstr "LibraryThing für Bibliotheken - http://www.librarything.com/forlibraries"

#: ../../source/links.rst:84
msgid "Design related"
msgstr "In Bezug auf das Design"

#: ../../source/links.rst:86
msgid "JQuery Library - http://wiki.koha-community.org/wiki/JQuery_Library"
msgstr "JQuery-Bibliothek - http://wiki.koha-community.org/wiki/JQuery_Library"

#: ../../source/links.rst:88
msgid "HTML & CSS Library - http://wiki.koha-community.org/wiki/HTML_%26_CSS_Library"
msgstr "HTML & CSS Library - http://wiki.koha-community.org/wiki/HTML_%26_CSS_Library"

#: ../../source/links.rst:91
msgid "Owen Leonard's Koha Blog - http://www.myacpl.org/koha"
msgstr "Owen Leonards Koha-Blog - http://www.myacpl.org/koha"

#: ../../source/links.rst:96
msgid "Reports related"
msgstr "In Bezug auf Reports"

#: ../../source/links.rst:98
msgid "SQL Reports Library - http://wiki.koha-community.org/wiki/SQL_Reports_Library"
msgstr "SQL-Reports-Bibliothek - http://wiki.koha-community.org/wiki/SQL_Reports_Library"

#: ../../source/links.rst:101
msgid "Database Schema - http://schema.koha-community.org"
msgstr "Datenbankschema - http://schema.koha-community.org"

#: ../../source/links.rst:103
msgid "Sample reports from NEKLS - http://www.nexpresslibrary.org/training/reports-training/"
msgstr "Reports von NEKLS - http://www.nexpresslibrary.org/training/reports-training/"

#: ../../source/links.rst:109
msgid "Installation guides"
msgstr "Installationsanleitungen"

#: ../../source/links.rst:111
msgid "Installing Koha on Debian - https://wiki.koha-community.org/wiki/Koha_on_Debian"
msgstr "Installation von Koha unter Debian - https://wiki.koha-community.org/wiki/Koha_on_Debian"

#: ../../source/links.rst:114
msgid "Installing Koha on Raspberry Pi 2 - https://wiki.koha-community.org/wiki/Koha_on_a_Raspberry_Pi_2"
msgstr "Installation von Koha auf Raspberry Pi 2 - https://wiki.koha-community.org/wiki/Koha_on_a_Raspberry_Pi_2"

#: ../../source/links.rst:117
msgid "Installing Koha on Ubuntu - https://wiki.koha-community.org/wiki/Koha_on_Ubuntu"
msgstr "Installation von Koha unter Ubuntu - https://wiki.koha-community.org/wiki/Koha_on_Ubuntu"

#: ../../source/links.rst:123
msgid "Misc"
msgstr "Verschiedenes"

#: ../../source/links.rst:125
msgid "Zotero - http://zotero.org"
msgstr "Zotero - http://zotero.org"
