# Italian translations for Koha Manual package.
# Copyright (C) 2020, Koha Community
# This file is distributed under the same license as the Koha Manual package.
# Automatically generated, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: Koha Manual 22.11\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-12-12 01:03+0000\n"
"PO-Revision-Date: 2021-05-01 11:56+0000\n"
"Last-Translator: ztajoli <ztajoli@gmail.com>\n"
"Language-Team: none\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Pootle 2.8\n"
"X-Pootle-Path: /it/manual22.11/localusepreferences.po\n"
"X-Pootle-Revision: 1\n"
"X-POOTLE-MTIME: 1619870186.824012\n"

#: ../../source/localusepreferences.rst:6
#, fuzzy
msgid "Local use"
msgstr "Uso locale"

#: ../../source/localusepreferences.rst:8
msgid "These preferences are defined locally."
msgstr "Queste preferenze sono definite localmente."

#: ../../source/localusepreferences.rst:10
#, fuzzy
msgid "*Get there:* More > Administration > System preferences > Local Use"
msgstr "*Vai a:* Più > Amministrazione > Preferenze di sistema globali > Uso locale"

#: ../../source/localusepreferences.rst:13
msgid "**Note**"
msgstr "**Nota**"

#: ../../source/localusepreferences.rst:15
msgid "Sometimes preferences which are either new or outdated will appear in this tab, if you didn't add any preferences to this tab then it's best to ignore preferences listed here."
msgstr "A volte le preferenze che sono nuove o non aggiornate verranno visualizzate in questa scheda, se non è stato aggiunta alcun preferenze a questa scheda, allora è meglio ignorare le preferenze elencati qui."

#: ../../source/localusepreferences.rst:22
msgid "INTRAdidyoumean"
msgstr "INTRAdidyoumean"

#: ../../source/localusepreferences.rst:24
#: ../../source/localusepreferences.rst:34
#: ../../source/localusepreferences.rst:59
#: ../../source/localusepreferences.rst:69
#: ../../source/localusepreferences.rst:79
msgid "Default: blank"
msgstr "Predefinito: vuoto"

#: ../../source/localusepreferences.rst:26
msgid "Asks: Did you mean? configuration for the Intranet. Do not change, as this is controlled by /cgi-bin/koha/admin/didyoumean.pl."
msgstr "Chiede: la configurazione della funzione \"Intendi dire?\" per l'interfaccia dello staff. Non modificarla, dato che viene controllata da /cgi-bin/koha/admin/didyoumean.pl."

#: ../../source/localusepreferences.rst:32
msgid "OPACdidyoumean"
msgstr "OPACdidyoumean"

#: ../../source/localusepreferences.rst:36
msgid "Asks: Did you mean? configuration for the OPAC. Do not change, as this is controlled by /cgi-bin/koha/admin/didyoumean.pl."
msgstr "Chiede: la configurazione della funzione \"Intendi dire?\" per l'OPAC. Non modificarla, dato che viene controllata da /cgi-bin/koha/admin/didyoumean.pl."

#: ../../source/localusepreferences.rst:42
msgid "printcirculationships"
msgstr "printcirculationships"

#: ../../source/localusepreferences.rst:44
msgid "Default: ON"
msgstr "Default: ON"

#: ../../source/localusepreferences.rst:46
msgid "Asks: If ON, enable printing circulation receipts"
msgstr "Chiede: se attivo (ON), attiva la stampa delle ricevute di prestito"

#: ../../source/localusepreferences.rst:48
msgid "Values:"
msgstr "Valori:"

#: ../../source/localusepreferences.rst:50
msgid "ON"
msgstr "ON"

#: ../../source/localusepreferences.rst:52
msgid "OFF"
msgstr "OFF"

#: ../../source/localusepreferences.rst:57
msgid "UsageStatsID"
msgstr "UsageStatsID"

#: ../../source/localusepreferences.rst:61
#: ../../source/localusepreferences.rst:71
msgid "Asks: This preference is part of Koha but it should not be deleted or updated manually."
msgstr "Chiede: questa preferenza è parte di Koha ma non deve essere cancellata o modificata manualmente."

#: ../../source/localusepreferences.rst:67
msgid "UsageStatsLastUpdateTime"
msgstr "UsageStatsLastUpdateTime"

#: ../../source/localusepreferences.rst:77
msgid "UsageStatsPublicID"
msgstr "UsageStatsPublicID"

#: ../../source/localusepreferences.rst:81
msgid "Asks: Public ID for Hea website"
msgstr "Chiede: ID pubblico per il sito web Hea"

#: ../../source/localusepreferences.rst:86
msgid "Version"
msgstr "Versione"

#: ../../source/localusepreferences.rst:88
msgid "Default: automatically generated"
msgstr "Predefinito: generato automaticamente"

#: ../../source/localusepreferences.rst:90
msgid "Asks: The Koha database version. WARNING: Do not change this value manually. It is maintained by the webinstaller"
msgstr "Chiede: La versione del database di Koha. ATTENZIONE: questo valore non va modificato manualmente. È mantenuto dal webinstaller"
