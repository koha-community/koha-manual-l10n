# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2022-23, Koha Community
# This file is distributed under the same license as the Koha Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2024.
#
msgid ""
msgstr ""
"Project-Id-Version: Koha Manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-12-01 01:01+0000\n"
"PO-Revision-Date: 2024-07-10 01:00+0000\n"
"Last-Translator: dataly <gveranis@dataly.gr>\n"
"Language-Team: Greek <https://translate.koha-community.org/projects/koha-manual/links/el/>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.6.2\n"
"Generated-By: Babel 2.8.0\n"

#: ../../source/links.rst:6
msgid "Important links"
msgstr "Σημαντικοί σύνδεσμοι"

#: ../../source/links.rst:11
msgid "Koha related"
msgstr "Σχετικά με το Koha"

#: ../../source/links.rst:13
msgid "Report Koha Bugs - http://bugs.koha-community.org"
msgstr "Αναφορά σφαλμάτων Koha - http://bugs.koha-community.org"

#: ../../source/links.rst:15
msgid "Koha Versioning Control - http://git.koha-community.org/"
msgstr "Έλεγχος έκδοσης Koha - http://git.koha-community.org/"

#: ../../source/links.rst:17
msgid "Database Structure - http://schema.koha-community.org"
msgstr "Δομή βάσης δεδομένων - http://schema.koha-community.org"

#: ../../source/links.rst:19
msgid "Koha Community Statistics - http://hea.koha-community.org"
msgstr "Κοινοτικές στατιστικές Koha - http://hea.koha-community.org"

#: ../../source/links.rst:21
msgid "Koha as a CMS - http://wiki.koha-community.org/wiki/Koha_as_a_CMS"
msgstr "Το Koha ως CMS - http://wiki.koha-community.org/wiki/Koha_as_a_CMS"

#: ../../source/links.rst:23
msgid "Koha Bibliography - http://www.zotero.org/groups/koha"
msgstr "Βιβλιογραφία Koha - http://www.zotero.org/groups/koha"

#: ../../source/links.rst:25
msgid "Koha Shared Links - http://groups.diigo.com/group/everything-koha"
msgstr "Κοινόχρηστοι σύνδεσμοι Koha - http://groups.diigo.com/group/everything-koha"

#: ../../source/links.rst:30
msgid "Circulation related"
msgstr "Σχετικά με την κυκλοφορία υλικού"

#: ../../source/links.rst:32
msgid "Koha Desktop Offline Circulation: https://github.com/bywatersolutions/koha-offline-circulation/releases"
msgstr "Koha Κυκλοφορία υλικού εκτός σύνδεσης από υπολογιστή : https://github.com/bywatersolutions/koha-offline-circulation/releases"

#: ../../source/links.rst:35
msgid "Koha Firefox Offline Circulation: https://addons.mozilla.org/en-US/firefox/addon/koct/"
msgstr "Koha Κυκλοφορία υλικού εκτός σύνδεσης από Firefox: https://addons.mozilla.org/en-US/firefox/addon/koct/"

#: ../../source/links.rst:41
msgid "Cataloging related"
msgstr "Σχετικά με την καταλογογράφηση"

#: ../../source/links.rst:43
msgid "Koha MARC Tutorials - http://www.pakban.net/brooke/"
msgstr "Koha MARC παραδείγματα χρήσης  - http://www.pakban.net/brooke/"

#: ../../source/links.rst:45
msgid "IRSpy Open Z39.50 Server Search - http://irspy.indexdata.com/"
msgstr "Αναζήτηση διακομιστή IRSpy Open Z39.50 - http://irspy.indexdata.com/"

#: ../../source/links.rst:47
msgid "Z39.50 Server List - http://staff.library.mun.ca/staff/toolbox/z3950hosts.htm"
msgstr "Κατάλογος διακομιστών Z39.50 - http://staff.library.mun.ca/staff/toolbox/z3950hosts.htm"

#: ../../source/links.rst:50
msgid "Open Koha Z39.50 Targets - http://wiki.koha-community.org/wiki/Koha_Open_Z39.50_Sources"
msgstr "Ανοιχτοί Koha Z39.50 διακομιστές - http://wiki.koha-community.org/wiki/Koha_Open_Z39.50_Sources"

#: ../../source/links.rst:53
msgid "Library of Congress Authorities - http://authorities.loc.gov/"
msgstr "Βιβλιοθήκη του Κογκρέσου - http://authorities.loc.gov/"

#: ../../source/links.rst:55
msgid "MARC Country Codes - http://www.loc.gov/marc/countries/"
msgstr "Κωδικοί χωρών MARC - http://www.loc.gov/marc/countries/"

#: ../../source/links.rst:57
msgid "Search the MARC Code List for Organizations - http://www.loc.gov/marc/organizations/org-search.php"
msgstr "Αναζήτηση στον κατάλογο κωδικών MARC για οργανισμούς - http://www.loc.gov/marc/organizations/org-search.php"

#: ../../source/links.rst:60
msgid "Search for Canadian MARC Codes - http://www.collectionscanada.gc.ca/illcandir-bin/illsear/l=0/c=1"
msgstr "Αναζήτηση για καναδικούς κωδικούς MARC - http://www.collectionscanada.gc.ca/illcandir-bin/illsear/l=0/c=1"

#: ../../source/links.rst:63
msgid "Z39.50 Bib-1 Attribute - http://www.loc.gov/z3950/agency/defns/bib1.html"
msgstr "Z39.50 Χαρακτηριστικό Bib-1 - http://www.loc.gov/z3950/agency/defns/bib1.html"

#: ../../source/links.rst:69
msgid "Enhanced content related"
msgstr "Σχετικά με το ενισχυμένο περιεχόμενο"

#: ../../source/links.rst:71
msgid "Amazon Associates - `https://affiliate-program.amazon.com <https://affiliate-program.amazon.com/>`__"
msgstr "Amazon Associates - `https://affiliate-program.amazon.com <https://affiliate-program.amazon.com/>`__"

#: ../../source/links.rst:74
msgid "Amazon Web Services - http://aws.amazon.com"
msgstr "Amazon Web Services - http://aws.amazon.com"

#: ../../source/links.rst:76
msgid "WorldCat Affiliate Tools - http://www.worldcat.org/wcpa/do/AffiliateUserServices?method=initSelfRegister"
msgstr "Εργαλεία συνεργατών του WorldCat - http://www.worldcat.org/wcpa/do/AffiliateUserServices?method=initSelfRegister"

#: ../../source/links.rst:79
msgid "LibraryThing for Libraries - http://www.librarything.com/forlibraries"
msgstr "LibraryThing για βιβλιοθήκες - http://www.librarything.com/forlibraries"

#: ../../source/links.rst:84
msgid "Design related"
msgstr "Σχετικά με τον σχεδιασμό"

#: ../../source/links.rst:86
msgid "JQuery Library - http://wiki.koha-community.org/wiki/JQuery_Library"
msgstr "Βιβλιοθήκη JQuery - http://wiki.koha-community.org/wiki/JQuery_Library"

#: ../../source/links.rst:88
msgid "HTML & CSS Library - http://wiki.koha-community.org/wiki/HTML_%26_CSS_Library"
msgstr "Βιβλιοθήκη HTML & CSS - http://wiki.koha-community.org/wiki/HTML_%26_CSS_Library"

#: ../../source/links.rst:91
msgid "Owen Leonard's Koha Blog - http://www.myacpl.org/koha"
msgstr "Το ιστολόγιο Koha του Owen Leonard - http://www.myacpl.org/koha"

#: ../../source/links.rst:96
msgid "Reports related"
msgstr "Σχετικά με τις εκθέσεις"

#: ../../source/links.rst:98
msgid "SQL Reports Library - http://wiki.koha-community.org/wiki/SQL_Reports_Library"
msgstr "Βιβλιοθήκη αναφορών SQL - http://wiki.koha-community.org/wiki/SQL_Reports_Library"

#: ../../source/links.rst:101
msgid "Database Schema - http://schema.koha-community.org"
msgstr "Σχήμα βάσης δεδομένων - http://schema.koha-community.org"

#: ../../source/links.rst:103
msgid "Sample reports from NEKLS - http://www.nexpresslibrary.org/training/reports-training/"
msgstr "Δείγματα αναφορών από το NEKLS - http://www.nexpresslibrary.org/training/reports-training/"

#: ../../source/links.rst:109
msgid "Installation guides"
msgstr "Οδηγοί εγκατάστασης"

#: ../../source/links.rst:111
msgid "Installing Koha on Debian - https://wiki.koha-community.org/wiki/Koha_on_Debian"
msgstr "Εγκατάσταση του Koha στο Debian - https://wiki.koha-community.org/wiki/Koha_on_Debian"

#: ../../source/links.rst:114
msgid "Installing Koha on Raspberry Pi 2 - https://wiki.koha-community.org/wiki/Koha_on_a_Raspberry_Pi_2"
msgstr "Εγκατάσταση του Koha στο Raspberry Pi 2 - https://wiki.koha-community.org/wiki/Koha_on_a_Raspberry_Pi_2"

#: ../../source/links.rst:117
msgid "Installing Koha on Ubuntu - https://wiki.koha-community.org/wiki/Koha_on_Ubuntu"
msgstr "Εγκατάσταση του Koha στο Ubuntu - https://wiki.koha-community.org/wiki/Koha_on_Ubuntu"

#: ../../source/links.rst:123
msgid "Misc"
msgstr "Διάφορα"

#: ../../source/links.rst:125
msgid "Zotero - http://zotero.org"
msgstr "Zotero - http://zotero.org"
