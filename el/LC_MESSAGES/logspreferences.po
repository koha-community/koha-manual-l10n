# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2022-23, Koha Community
# This file is distributed under the same license as the Koha Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2024.
#
msgid ""
msgstr ""
"Project-Id-Version: Koha Manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2025-02-12 01:00+0000\n"
"PO-Revision-Date: 2025-02-28 01:00+0000\n"
"Last-Translator: Gouni Anna <a.gouni@dataly.gr>\n"
"Language-Team: Greek <https://translate.koha-community.org/projects/koha-manual/logspreferences/el/>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.6.2\n"
"Generated-By: Babel 2.8.0\n"

#: ../../source/logspreferences.rst:6
msgid "Logs"
msgstr ""

#: ../../source/logspreferences.rst:8
msgid "Logs keep track of transactions in the system. You can decide which actions you want to log and which you don't using these preferences. Logs can then be viewed in the :ref:`Log viewer tool<log-viewer-label>`."
msgstr ""

#: ../../source/logspreferences.rst:12
msgid "*Get there:* More > Administration > System preferences > Logs"
msgstr ""

#: ../../source/logspreferences.rst:17
msgid "Debugging"
msgstr ""

#: ../../source/logspreferences.rst:22
msgid "ActionLogsTraceDepth"
msgstr ""

#: ../../source/logspreferences.rst:24
msgid "Asks: When logging actions, store a stack trace that goes at most \\_\\_\\_ levels deep."
msgstr ""

#: ../../source/logspreferences.rst:27
msgid "Default: 0"
msgstr "Προεπιλογή: 0"

#: ../../source/logspreferences.rst:29 ../../source/logspreferences.rst:57
#: ../../source/logspreferences.rst:84 ../../source/logspreferences.rst:112
#: ../../source/logspreferences.rst:144 ../../source/logspreferences.rst:242
#: ../../source/logspreferences.rst:271 ../../source/logspreferences.rst:305
#: ../../source/logspreferences.rst:334 ../../source/logspreferences.rst:382
#: ../../source/logspreferences.rst:423 ../../source/logspreferences.rst:452
#: ../../source/logspreferences.rst:480 ../../source/logspreferences.rst:518
#: ../../source/logspreferences.rst:562 ../../source/logspreferences.rst:594
#: ../../source/logspreferences.rst:623 ../../source/logspreferences.rst:659
#: ../../source/logspreferences.rst:693 ../../source/logspreferences.rst:729
#: ../../source/logspreferences.rst:757 ../../source/logspreferences.rst:791
#: ../../source/logspreferences.rst:819 ../../source/logspreferences.rst:859
msgid "Description:"
msgstr "Περιγραφή:"

#: ../../source/logspreferences.rst:31
msgid "This system preference allows you to store in the action\\_logs table the information about where in Koha a particular :ref:`logged action <logging-label>` was generated from, and how."
msgstr ""

#: ../../source/logspreferences.rst:35
msgid "Use 0 to deactivate this system preference."
msgstr ""

#: ../../source/logspreferences.rst:39
msgid "This information is only stored in the database, it is not visible in the :ref:`log viewer tool <log-viewer-label>`."
msgstr ""

#: ../../source/logspreferences.rst:45
msgid "DumpSearchQueryTemplate"
msgstr ""

#: ../../source/logspreferences.rst:47
msgid "Asks: \\_\\_\\_ dump search query as a template parameter."
msgstr ""

#: ../../source/logspreferences.rst:49 ../../source/logspreferences.rst:76
#: ../../source/logspreferences.rst:104
msgid "Default: Don't"
msgstr "Προεπιλογή: Να μην γίνεται"

#: ../../source/logspreferences.rst:51 ../../source/logspreferences.rst:78
#: ../../source/logspreferences.rst:106
msgid "Value:"
msgstr "Τιμή:"

#: ../../source/logspreferences.rst:53 ../../source/logspreferences.rst:80
#: ../../source/logspreferences.rst:108
msgid "Don't"
msgstr "Να μην γίνεται"

#: ../../source/logspreferences.rst:55 ../../source/logspreferences.rst:82
#: ../../source/logspreferences.rst:110
msgid "Do"
msgstr "Να γίνει"

#: ../../source/logspreferences.rst:59
msgid "This system preference allows you to view the search query used by Zebra or Elasticsearch, to help with troubleshooting."
msgstr ""

#: ../../source/logspreferences.rst:62
msgid "Make sure to enable :ref:`DumpTemplateVarsIntranet <DumpTemplateVarsIntranet-label>` and/or :ref:`DumpTemplateVarsOpac <DumpTemplateVarsOpac-label>` otherwise this system preference will have no effect."
msgstr ""

#: ../../source/logspreferences.rst:66
msgid "The dumped query will be in the page source under 'search_query'."
msgstr ""

#: ../../source/logspreferences.rst:71
msgid "DumpTemplateVarsIntranet"
msgstr ""

#: ../../source/logspreferences.rst:73
msgid "Asks: \\_\\_\\_ dump all Template Toolkit variable to a comment in the HTML source for the staff interface."
msgstr ""

#: ../../source/logspreferences.rst:86
msgid "This system preference allows you to view the Template Toolkit variables used on a staff interface page, to help with troubleshooting."
msgstr ""

#: ../../source/logspreferences.rst:89 ../../source/logspreferences.rst:117
msgid "The dumped information will be in the page source, in a comment at the beginning of the file."
msgstr ""

#: ../../source/logspreferences.rst:92 ../../source/logspreferences.rst:120
msgid "See also:"
msgstr "Δείτε επίσης:"

#: ../../source/logspreferences.rst:94
msgid ":ref:`DumpTemplateVarsOpac <DumpTemplateVarsOpac-label>`"
msgstr ""

#: ../../source/logspreferences.rst:99
msgid "DumpTemplateVarsOpac"
msgstr ""

#: ../../source/logspreferences.rst:101
msgid "Asks: \\_\\_\\_ dump all Template Toolkit variable to a comment in the HTML source for the OPAC."
msgstr ""

#: ../../source/logspreferences.rst:114
msgid "This system preference allows you to view the Template Toolkit variables used on an OPAC page, to help with troubleshooting."
msgstr ""

#: ../../source/logspreferences.rst:122
msgid ":ref:`DumpTemplateVarsIntranet <DumpTemplateVarsIntranet-label>`"
msgstr ""

#: ../../source/logspreferences.rst:127
msgid "Logging"
msgstr ""

#: ../../source/logspreferences.rst:132
msgid "AcquisitionLog"
msgstr ""

#: ../../source/logspreferences.rst:134
msgid "Asks: \\_\\_\\_ when acquisition actions take place."
msgstr ""

#: ../../source/logspreferences.rst:136 ../../source/logspreferences.rst:234
#: ../../source/logspreferences.rst:297 ../../source/logspreferences.rst:444
#: ../../source/logspreferences.rst:510 ../../source/logspreferences.rst:554
#: ../../source/logspreferences.rst:615 ../../source/logspreferences.rst:651
#: ../../source/logspreferences.rst:721 ../../source/logspreferences.rst:749
#: ../../source/logspreferences.rst:857
msgid "Default: Don't log"
msgstr ""

#: ../../source/logspreferences.rst:138 ../../source/logspreferences.rst:236
#: ../../source/logspreferences.rst:265 ../../source/logspreferences.rst:299
#: ../../source/logspreferences.rst:328 ../../source/logspreferences.rst:376
#: ../../source/logspreferences.rst:417 ../../source/logspreferences.rst:446
#: ../../source/logspreferences.rst:474 ../../source/logspreferences.rst:512
#: ../../source/logspreferences.rst:556 ../../source/logspreferences.rst:588
#: ../../source/logspreferences.rst:617 ../../source/logspreferences.rst:653
#: ../../source/logspreferences.rst:687 ../../source/logspreferences.rst:723
#: ../../source/logspreferences.rst:751 ../../source/logspreferences.rst:785
#: ../../source/logspreferences.rst:813 ../../source/logspreferences.rst:851
msgid "Values:"
msgstr "Τιμές:"

#: ../../source/logspreferences.rst:140 ../../source/logspreferences.rst:238
#: ../../source/logspreferences.rst:267 ../../source/logspreferences.rst:301
#: ../../source/logspreferences.rst:330 ../../source/logspreferences.rst:378
#: ../../source/logspreferences.rst:419 ../../source/logspreferences.rst:448
#: ../../source/logspreferences.rst:476 ../../source/logspreferences.rst:514
#: ../../source/logspreferences.rst:558 ../../source/logspreferences.rst:590
#: ../../source/logspreferences.rst:619 ../../source/logspreferences.rst:655
#: ../../source/logspreferences.rst:689 ../../source/logspreferences.rst:725
#: ../../source/logspreferences.rst:753 ../../source/logspreferences.rst:787
#: ../../source/logspreferences.rst:815 ../../source/logspreferences.rst:853
msgid "Don't log"
msgstr ""

#: ../../source/logspreferences.rst:142 ../../source/logspreferences.rst:240
#: ../../source/logspreferences.rst:269 ../../source/logspreferences.rst:303
#: ../../source/logspreferences.rst:332 ../../source/logspreferences.rst:380
#: ../../source/logspreferences.rst:421 ../../source/logspreferences.rst:450
#: ../../source/logspreferences.rst:478 ../../source/logspreferences.rst:516
#: ../../source/logspreferences.rst:560 ../../source/logspreferences.rst:592
#: ../../source/logspreferences.rst:621 ../../source/logspreferences.rst:657
#: ../../source/logspreferences.rst:691 ../../source/logspreferences.rst:727
#: ../../source/logspreferences.rst:755 ../../source/logspreferences.rst:789
#: ../../source/logspreferences.rst:817 ../../source/logspreferences.rst:855
msgid "Log"
msgstr ""

#: ../../source/logspreferences.rst:146
msgid "This system preference controls whether or not Koha will log various actions done in the :ref:`acquisitions module<acquisitions-label>`. These actions include:"
msgstr ""

#: ../../source/logspreferences.rst:150
msgid "Adding new baskets"
msgstr ""

#: ../../source/logspreferences.rst:152
msgid "Re-opening closed baskets"
msgstr ""

#: ../../source/logspreferences.rst:154
msgid "Modifying baskets"
msgstr ""

#: ../../source/logspreferences.rst:156
msgid "Modifying basket headers"
msgstr ""

#: ../../source/logspreferences.rst:158
msgid "Modifying basket users"
msgstr ""

#: ../../source/logspreferences.rst:160
msgid "Closing baskets"
msgstr ""

#: ../../source/logspreferences.rst:162
msgid "Approving baskets"
msgstr ""

#: ../../source/logspreferences.rst:164
msgid "Creating a new order line"
msgstr ""

#: ../../source/logspreferences.rst:166
msgid "Cancelling an order line"
msgstr ""

#: ../../source/logspreferences.rst:168
msgid "Adding an invoice adjustment"
msgstr ""

#: ../../source/logspreferences.rst:170
msgid "Editing an invoice adjustment"
msgstr ""

#: ../../source/logspreferences.rst:172
msgid "Deleting an invoice adjustment"
msgstr ""

#: ../../source/logspreferences.rst:174
msgid "Receiving an order line against an invoice"
msgstr ""

#: ../../source/logspreferences.rst:176
msgid "Editing a budget"
msgstr "Επεξεργασία ενός προϋπολογισμού"

#: ../../source/logspreferences.rst:178
msgid "Editing a fund"
msgstr ""

#: ../../source/logspreferences.rst:180
msgid "Order release date (EDIFACT)"
msgstr ""

#: ../../source/logspreferences.rst:182 ../../source/logspreferences.rst:248
#: ../../source/logspreferences.rst:276 ../../source/logspreferences.rst:311
#: ../../source/logspreferences.rst:339 ../../source/logspreferences.rst:387
#: ../../source/logspreferences.rst:429 ../../source/logspreferences.rst:457
#: ../../source/logspreferences.rst:485 ../../source/logspreferences.rst:523
#: ../../source/logspreferences.rst:567 ../../source/logspreferences.rst:599
#: ../../source/logspreferences.rst:630 ../../source/logspreferences.rst:664
#: ../../source/logspreferences.rst:698 ../../source/logspreferences.rst:734
#: ../../source/logspreferences.rst:762 ../../source/logspreferences.rst:796
#: ../../source/logspreferences.rst:824 ../../source/logspreferences.rst:865
msgid "These logs are searchable through the :ref:`log viewer tool <log-viewer-label>`."
msgstr ""

#: ../../source/logspreferences.rst:184 ../../source/logspreferences.rst:250
#: ../../source/logspreferences.rst:278 ../../source/logspreferences.rst:313
#: ../../source/logspreferences.rst:341 ../../source/logspreferences.rst:389
#: ../../source/logspreferences.rst:431 ../../source/logspreferences.rst:459
#: ../../source/logspreferences.rst:487 ../../source/logspreferences.rst:525
#: ../../source/logspreferences.rst:569 ../../source/logspreferences.rst:601
#: ../../source/logspreferences.rst:632 ../../source/logspreferences.rst:666
#: ../../source/logspreferences.rst:700 ../../source/logspreferences.rst:736
#: ../../source/logspreferences.rst:764 ../../source/logspreferences.rst:798
#: ../../source/logspreferences.rst:826 ../../source/logspreferences.rst:867
msgid "These logs are kept in the action\\_logs table in the database."
msgstr ""

#: ../../source/logspreferences.rst:186
msgid "The module is 'ACQUISITIONS'."
msgstr ""

#: ../../source/logspreferences.rst:188 ../../source/logspreferences.rst:282
#: ../../source/logspreferences.rst:345 ../../source/logspreferences.rst:393
#: ../../source/logspreferences.rst:491 ../../source/logspreferences.rst:529
#: ../../source/logspreferences.rst:573 ../../source/logspreferences.rst:636
#: ../../source/logspreferences.rst:670 ../../source/logspreferences.rst:704
#: ../../source/logspreferences.rst:768 ../../source/logspreferences.rst:830
msgid "Possible actions are"
msgstr ""

#: ../../source/logspreferences.rst:190
msgid "ADD\\_BASKET: a :ref:`new basket was created<create-a-basket-label>`"
msgstr "ADD\\_BASKET: a :ref:`Δημιουργήθηκε νέο καλάθι<create-a-basket-label>`"

#: ../../source/logspreferences.rst:192
msgid "APPROVE\\_BASKET: an EDI basket was approved"
msgstr "APPROVE\\_BASKET: ένα καλάθι EDI εγκρίθηκε"

#: ../../source/logspreferences.rst:194
msgid "CANCEL\\_ORDER: an order was cancelled"
msgstr "CANCEL\\_ORDER: μια παραγγελία ακυρώθηκε"

#: ../../source/logspreferences.rst:196
msgid "CLOSE\\_BASKET: a :ref:`basket was closed<ordering-closing-basket-label>`"
msgstr "CLOSE\\_BASKET: το :ref:`καλάθι έκλεισε<ordering-closing-basket-label>`"

#: ../../source/logspreferences.rst:198
msgid "CREATE\\_FUND: a :ref:`fund was created<add-a-fund-label>`"
msgstr "CREATE\\_FUND: ένα :ref:`κεφάλαιο δημιουργήθηκε <add-a-fund-label>`"

#: ../../source/logspreferences.rst:200
msgid "CREATE\\_INVOICE\\_ADJUSTMENT: an adjustment was added to an invoice"
msgstr "CREATE\\_INVOICE\\_ADJUSTMENT: προστέθηκε μια τροποποίηση σε ένα τιμολόγιο"

#: ../../source/logspreferences.rst:202
msgid "CREATE\\_ORDER: an :ref:`order was added to a basket<add-to-basket-label>`"
msgstr "CREATE\\_ORDER: μία :ref:`παραγγελία προστέθηκε σε ένα καλάθι <add-to-basket-label>`"

#: ../../source/logspreferences.rst:204
msgid "DELETE\\_FUND: a fund was deleted"
msgstr "DELETE\\_FUND: ένα κεφάλαιο διαγράφηκε"

#: ../../source/logspreferences.rst:206
msgid "DELETE\\_INVOICE\\_ADJUSTMENT: an adjustment was deleted from an invoice"
msgstr "DELETE\\_INVOICE\\_ADJUSTMENT: μια τροποποίηση διαγράφηκε από ένα τιμολόγιο"

#: ../../source/logspreferences.rst:208
msgid "MODIFY\\_BASKET: a :ref:`basket was edited<edit-basket-label>` (adding or modifying orders)"
msgstr "MODIFY\\_BASKET: ένα :ref:`καλάθι επεξεργάστηκε <edit-basket-label>` (προσθήκη ή τροποποίηση παραγγελιών)"

#: ../../source/logspreferences.rst:210
msgid "MODIFY\\_BASKET\\_HEADER: a basket's information (such as the basket name or billing place) was edited"
msgstr "MODIFY\\_BASKET\\_HEADER: επεξεργασία των πληροφοριών ενός καλαθιού (όπως το όνομα του καλαθιού ή ο τόπος χρέωσης)"

#: ../../source/logspreferences.rst:213
msgid "MODIFY\\_BASKET\\_USERS: a basket's users were edited"
msgstr "MODIFY\\_BASKET\\_USERS: επεξεργασία των χρηστών ενός καλαθιού"

#: ../../source/logspreferences.rst:215
msgid "MODIFY\\_BUDGET: a budget was edited (this does not include clo)"
msgstr ""

#: ../../source/logspreferences.rst:217
msgid "MODIFY\\_FUND: a fund was edited"
msgstr ""

#: ../../source/logspreferences.rst:219
msgid "MODIFY\\_ORDER: an order was edited"
msgstr ""

#: ../../source/logspreferences.rst:221
msgid "RECEIVE\\_ORDER: an :ref:`order was received<receiving-orders-label>`"
msgstr ""

#: ../../source/logspreferences.rst:223
msgid "REOPEN\\_BASKET: a closed basket was reopened"
msgstr ""

#: ../../source/logspreferences.rst:225
msgid "UPDATE\\_INVOICE\\_ADJUSTMENT: an adjustment to an invoice was edited"
msgstr ""

#: ../../source/logspreferences.rst:230
msgid "AuthFailureLog"
msgstr ""

#: ../../source/logspreferences.rst:232
msgid "Asks: \\_\\_\\_ authentication failures."
msgstr ""

#: ../../source/logspreferences.rst:244
msgid "This system preference controls whether or not Koha should log when a patron fails to successfully authenticate on the OPAC (or a staff member in the staff interface)."
msgstr ""

#: ../../source/logspreferences.rst:252 ../../source/logspreferences.rst:315
msgid "The module is 'AUTH'"
msgstr ""

#: ../../source/logspreferences.rst:254
msgid "The action is 'FAILURE'"
msgstr ""

#: ../../source/logspreferences.rst:259
msgid "AuthoritiesLog"
msgstr ""

#: ../../source/logspreferences.rst:261
msgid "Asks: \\_\\_\\_ changes to authority records."
msgstr ""

#: ../../source/logspreferences.rst:263 ../../source/logspreferences.rst:326
#: ../../source/logspreferences.rst:374 ../../source/logspreferences.rst:415
#: ../../source/logspreferences.rst:472 ../../source/logspreferences.rst:586
#: ../../source/logspreferences.rst:685 ../../source/logspreferences.rst:783
#: ../../source/logspreferences.rst:811
msgid "Default: Log"
msgstr ""

#: ../../source/logspreferences.rst:273
msgid "This system preference controls whether or not Koha should log when an :ref:`authority record<authorities-label>` is created, modified or deleted."
msgstr ""

#: ../../source/logspreferences.rst:280
msgid "The module is 'AUTHORITIES'."
msgstr ""

#: ../../source/logspreferences.rst:284
msgid "ADD: authority record was created"
msgstr ""

#: ../../source/logspreferences.rst:286
msgid "DELETE: authority record was deleted"
msgstr ""

#: ../../source/logspreferences.rst:288
msgid "MODIFY: authority record was modified"
msgstr ""

#: ../../source/logspreferences.rst:293
msgid "AuthSuccessLog"
msgstr ""

#: ../../source/logspreferences.rst:295
msgid "Asks: \\_\\_\\_ successful authentications."
msgstr ""

#: ../../source/logspreferences.rst:307
msgid "This system preference controls whether or not Koha should log when a patron successfully authenticates on the OPAC (or a staff member in the staff interface)"
msgstr ""

#: ../../source/logspreferences.rst:317
msgid "The action is 'SUCCESS'"
msgstr ""

#: ../../source/logspreferences.rst:322
msgid "BorrowersLog"
msgstr ""

#: ../../source/logspreferences.rst:324
msgid "Asks: \\_\\_\\_ changes to patron records."
msgstr ""

#: ../../source/logspreferences.rst:336
msgid "This system preference controls whether or not Koha should log when a patron's account is edited."
msgstr ""

#: ../../source/logspreferences.rst:343
msgid "The module is 'MEMBERS'."
msgstr ""

#: ../../source/logspreferences.rst:347
msgid "ADDCIRCMESSAGE: an internal message or OPAC message was added to the patron's account"
msgstr ""

#: ../../source/logspreferences.rst:350
msgid "CHANGE PASS: a patron's password was changed"
msgstr ""

#: ../../source/logspreferences.rst:352
msgid "CREATE: a :ref:`new patron was added<add-a-new-patron-label>`"
msgstr ""

#: ../../source/logspreferences.rst:354
msgid "DELCIRCMESSAGE: an internal message or OPAC message was deleted"
msgstr ""

#: ../../source/logspreferences.rst:356
msgid "DELETE: a :ref:`patron's account was deleted<delete-patron-account-label>`"
msgstr ""

#: ../../source/logspreferences.rst:358
msgid "MODIFY: a :ref:`patron's account was edited<editing-patrons-label>`"
msgstr ""

#: ../../source/logspreferences.rst:360
msgid "RENEW: a :ref:`patron's membership was renewed<renew-patron-account-label>`"
msgstr ""

#: ../../source/logspreferences.rst:364
msgid "Enabling this system preference allows the tracking of cardnumber changes for patrons."
msgstr ""

#: ../../source/logspreferences.rst:370
msgid "CataloguingLog"
msgstr ""

#: ../../source/logspreferences.rst:372
msgid "Asks: \\_\\_\\_ any changes to bibliographic or item records."
msgstr ""

#: ../../source/logspreferences.rst:384
msgid "This system preference controls whether or not Koha should log when a bibliographic record or an item is created, modified or deleted."
msgstr ""

#: ../../source/logspreferences.rst:391
msgid "The module is 'CATALOGUING'."
msgstr ""

#: ../../source/logspreferences.rst:395
msgid "ADD: bibliographic record or item was created"
msgstr ""

#: ../../source/logspreferences.rst:397
msgid "DELETE: bibliographic record or item was deleted"
msgstr ""

#: ../../source/logspreferences.rst:399
msgid "MODIFY: bibliographic record or item was modified, or a cover image was added to the record"
msgstr ""

#: ../../source/logspreferences.rst:404
msgid "Since this occurs whenever a title is catalogued or edited, and the whole record is saved in the action\\_logs table, it can be very resource intensive, thus slowing down your system or taking a lot of space."
msgstr ""

#: ../../source/logspreferences.rst:411
msgid "ClaimsLog"
msgstr ""

#: ../../source/logspreferences.rst:413
msgid "Asks: \\_\\_\\ when an acquisitions claim or a serials claim notice is sent."
msgstr ""

#: ../../source/logspreferences.rst:425
msgid "This system preference controls whether or not Koha should log when a notice is sent for a :ref:`late order<claims-and-late-orders-label>` or a :ref:`late serial issue<claim-late-serials-label>`."
msgstr ""

#: ../../source/logspreferences.rst:433
msgid "The module is 'CLAIMS'."
msgstr ""

#: ../../source/logspreferences.rst:435
msgid "The action is 'ACQUISITION CLAIM'."
msgstr ""

#: ../../source/logspreferences.rst:440
msgid "CronjobLog"
msgstr ""

#: ../../source/logspreferences.rst:442
msgid "Asks: \\_\\_\\_ information from cron jobs."
msgstr ""

#: ../../source/logspreferences.rst:454
msgid "This system preference controls whether or not Koha should log when a :ref:`cron job<cron-jobs-label>` is run."
msgstr ""

#: ../../source/logspreferences.rst:461
msgid "The module is 'CRONJOBS'."
msgstr ""

#: ../../source/logspreferences.rst:463
msgid "The action is 'Run'."
msgstr ""

#: ../../source/logspreferences.rst:468
msgid "FinesLog"
msgstr ""

#: ../../source/logspreferences.rst:470
msgid "Asks: \\_\\_\\_ when fines are charged, paid or forgiven."
msgstr ""

#: ../../source/logspreferences.rst:482
msgid "This system preference controls whether or not Koha should log actions done on :ref:`charges<fines-label>` in patron's accounts."
msgstr ""

#: ../../source/logspreferences.rst:489
msgid "The module is 'FINES'."
msgstr ""

#: ../../source/logspreferences.rst:493
msgid "CREATE: a charge was added to a patron's account (manually or automatically)"
msgstr ""

#: ../../source/logspreferences.rst:496
msgid "MODIFY: a charge was modified (forgiven)"
msgstr ""

#: ../../source/logspreferences.rst:498
msgid "UPDATE: a charge was updated (only in the case of fines that are still accruing)"
msgstr ""

#: ../../source/logspreferences.rst:501
msgid "VOID: a :ref:`payment was voided<void-payments-label>`"
msgstr ""

#: ../../source/logspreferences.rst:506
msgid "HoldsLog"
msgstr ""

#: ../../source/logspreferences.rst:508
msgid "Asks: \\_\\_\\_ any actions on holds (create, cancel, suspend, resume, etc.)."
msgstr ""

#: ../../source/logspreferences.rst:520
msgid "This system preference controls whether or not Koha should log actions done on :ref:`holds<holds-circulation-label>`."
msgstr ""

#: ../../source/logspreferences.rst:527
msgid "The module is 'HOLDS'."
msgstr ""

#: ../../source/logspreferences.rst:531
msgid "CANCEL: a hold was cancelled"
msgstr ""

#: ../../source/logspreferences.rst:533
msgid "CREATE: a hold was placed"
msgstr ""

#: ../../source/logspreferences.rst:535
msgid "DELETE: a hold was deleted, an item on hold was checked out by the patron"
msgstr ""

#: ../../source/logspreferences.rst:538
msgid "FILL: a hold was :ref:`confirmed and put aside to await pickup<receiving-holds-label>`"
msgstr ""

#: ../../source/logspreferences.rst:540
msgid "MODIFY: a hold was modified (the priority was changed, the expiration date changed, etc.)"
msgstr ""

#: ../../source/logspreferences.rst:543
msgid "RESUME: a suspended hold was resumed"
msgstr ""

#: ../../source/logspreferences.rst:545
msgid "SUSPEND: a hold was suspended"
msgstr ""

#: ../../source/logspreferences.rst:550
msgid "IllLog"
msgstr ""

#: ../../source/logspreferences.rst:552
msgid "Asks: \\_\\_\\_ when changes to ILL requests take place."
msgstr ""

#: ../../source/logspreferences.rst:564
msgid "This system preference controls whether or not Koha should log changes on :ref:`ILL requests<ill-requests-label>`"
msgstr ""

#: ../../source/logspreferences.rst:571
msgid "The module is 'ILL'."
msgstr ""

#: ../../source/logspreferences.rst:575
msgid "PATRON\\_NOTICE: a notice was sent to a patron regarding their ILL request"
msgstr ""

#: ../../source/logspreferences.rst:577
msgid "STATUS\\_CHANGE: the status of an ILL request was modified"
msgstr ""

#: ../../source/logspreferences.rst:582
msgid "IssueLog"
msgstr ""

#: ../../source/logspreferences.rst:584
msgid "Asks: \\_\\_\\_ when items are checked out."
msgstr ""

#: ../../source/logspreferences.rst:596
msgid "This system preference controls whether or not Koha should log when :ref:`an item is checked out<check-out-(issuing)-label>`."
msgstr ""

#: ../../source/logspreferences.rst:603 ../../source/logspreferences.rst:738
#: ../../source/logspreferences.rst:800
msgid "The module is 'CIRCULATION'."
msgstr ""

#: ../../source/logspreferences.rst:605
msgid "The action is 'ISSUE'."
msgstr ""

#: ../../source/logspreferences.rst:610
msgid "NewsLog"
msgstr ""

#: ../../source/logspreferences.rst:612
msgid "Asks: \\_\\_\\_ changes to news entries and other contents managed in the news tool."
msgstr ""

#: ../../source/logspreferences.rst:625
msgid "This system preference controls whether or not Koha should log when a :ref:`news item<news-label>` or :ref:`HTML customization<html-customizations-label>` is created, modified or deleted."
msgstr ""

#: ../../source/logspreferences.rst:634
msgid "The module is 'NEWS'."
msgstr ""

#: ../../source/logspreferences.rst:638
msgid "ADD: a new news item or HTML customization was created"
msgstr ""

#: ../../source/logspreferences.rst:640
msgid "DELETE: a news item or HTML customization was deleted"
msgstr ""

#: ../../source/logspreferences.rst:642
msgid "MODIFY: a news item or HTML customization was edited"
msgstr ""

#: ../../source/logspreferences.rst:647
msgid "NoticesLog"
msgstr ""

#: ../../source/logspreferences.rst:649
msgid "Asks: \\_\\_\\_ changes to notice templates."
msgstr ""

#: ../../source/logspreferences.rst:661
msgid "This system preference controls whether or not Koha should log changes to :ref:`notice or slip templates<notices-and-slips-label>`."
msgstr ""

#: ../../source/logspreferences.rst:668
msgid "The module is 'NOTICES'."
msgstr ""

#: ../../source/logspreferences.rst:672
msgid "CREATE: a :ref:`new notice or slip template was created<adding-notices-and-slips-label>`"
msgstr ""

#: ../../source/logspreferences.rst:674
msgid "DELETE: a notice or slip template was deleted"
msgstr ""

#: ../../source/logspreferences.rst:676
msgid "MODIFY: a notice or slip template was edited"
msgstr ""

#: ../../source/logspreferences.rst:681
msgid "RecallsLog"
msgstr ""

#: ../../source/logspreferences.rst:683
msgid "Asks: \\_\\_\\_ any actions on recalls (create, cancel, expire, fulfill)."
msgstr ""

#: ../../source/logspreferences.rst:695
msgid "This system preference controls whether or not Koha should log actions related to recalls."
msgstr ""

#: ../../source/logspreferences.rst:702
msgid "The module is 'RECALLS'."
msgstr ""

#: ../../source/logspreferences.rst:706
msgid "CANCEL: a recall was cancelled"
msgstr ""

#: ../../source/logspreferences.rst:708
msgid "EXPIRE: a recall expired"
msgstr ""

#: ../../source/logspreferences.rst:710
msgid "FILL: a recall was filled (confirmed and set aside to wait for pickup)"
msgstr ""

#: ../../source/logspreferences.rst:712
msgid "OVERDUE: the status of a recall was set to 'overdue'"
msgstr ""

#: ../../source/logspreferences.rst:717
msgid "RenewalLog"
msgstr ""

#: ../../source/logspreferences.rst:719
msgid "Asks: \\_\\_\\_ when items are renewed."
msgstr ""

#: ../../source/logspreferences.rst:731
msgid "This system preference controls whether or not Koha should log when a :ref:`checkout is renewed<renewing-label>`."
msgstr ""

#: ../../source/logspreferences.rst:740
msgid "The action is 'RENEWAL'"
msgstr ""

#: ../../source/logspreferences.rst:745
msgid "ReportsLog"
msgstr ""

#: ../../source/logspreferences.rst:747
msgid "Asks: \\_\\_\\_ when reports are added, deleted or changed."
msgstr ""

#: ../../source/logspreferences.rst:759
msgid "This system preference controls whether or not Koha should log changes to :ref:`reports<reports-label>`."
msgstr ""

#: ../../source/logspreferences.rst:766
msgid "The module is 'REPORTS'."
msgstr ""

#: ../../source/logspreferences.rst:770
msgid "ADD: a new report was created"
msgstr ""

#: ../../source/logspreferences.rst:772
msgid "DELETE: a report was deleted"
msgstr ""

#: ../../source/logspreferences.rst:774
msgid "MODIFY: a report was edited"
msgstr ""

#: ../../source/logspreferences.rst:779
msgid "ReturnLog"
msgstr ""

#: ../../source/logspreferences.rst:781
msgid "Asks: \\_\\_\\_ when items are checked in."
msgstr ""

#: ../../source/logspreferences.rst:793
msgid "This system preference controls whether or not Koha should log when an :ref:`item is checked in<check-in-(returning)-label>`"
msgstr ""

#: ../../source/logspreferences.rst:802
msgid "The action is 'RETURN'"
msgstr ""

#: ../../source/logspreferences.rst:807
msgid "SubscriptionLog"
msgstr ""

#: ../../source/logspreferences.rst:809
msgid "Asks: \\_\\_\\_ when serials are added, deleted or changed."
msgstr ""

#: ../../source/logspreferences.rst:821
msgid "This system preference controls whether or not Koha should log changes to :ref:`serial subscriptions<serials-label>`."
msgstr ""

#: ../../source/logspreferences.rst:828
msgid "The module is 'SERIAL'."
msgstr ""

#: ../../source/logspreferences.rst:832
msgid "ADD: a new :ref:`serial subscription was created<add-a-subscription-label>`"
msgstr ""

#: ../../source/logspreferences.rst:834
msgid "DELETE: a serial subscription was deleted"
msgstr ""

#: ../../source/logspreferences.rst:836
msgid "MODIFY: a :ref:`serial subscription was edited<edit-subscription-label>`"
msgstr ""

#: ../../source/logspreferences.rst:838
msgid "RENEW: a :ref:`serial subscription was renewed<renewing-serials-label>`"
msgstr ""

#: ../../source/logspreferences.rst:843
msgid "SuggestionsLog"
msgstr ""

#: ../../source/logspreferences.rst:845
msgid "Version"
msgstr ""

#: ../../source/logspreferences.rst:847
msgid "This system preference was first introduced in version 24.11 of Koha."
msgstr ""

#: ../../source/logspreferences.rst:849
msgid "Asks: \\_\\_\\_ any changes to purchase suggestions (create, modify, delete)."
msgstr ""

#: ../../source/logspreferences.rst:861
msgid "This system preference controls whether Koha should keep a log of :ref:`purchase suggestions <managing-purchase-suggestions-label>` being added, updated and deleted. This is helpful when you need to troubleshoot a suggestion by checking its lifecycle."
msgstr ""

#: ../../source/logspreferences.rst:869
msgid "The module is 'SUGGESTION'."
msgstr ""

#: ../../source/logspreferences.rst:871
msgid "Possible actions are:"
msgstr ""

#: ../../source/logspreferences.rst:873
msgid "CREATE: a new suggestion was created;"
msgstr ""

#: ../../source/logspreferences.rst:875
msgid "MODIFY: a suggestion was edited (includes updated and archived);"
msgstr ""

#: ../../source/logspreferences.rst:877
msgid "DELETE: a suggestion was deleted."
msgstr ""
