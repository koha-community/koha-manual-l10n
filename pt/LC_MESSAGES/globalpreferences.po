# Portuguese translations for Koha Manual package.
# Copyright (C) 2020, Koha Community
# This file is distributed under the same license as the Koha Manual package.
# Automatically generated, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: Koha Manual 22.11\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2025-02-04 01:00+0000\n"
"PO-Revision-Date: 2024-03-06 01:00+0000\n"
"Last-Translator: vfernandes <vfernandes@keep.pt>\n"
"Language-Team: Portuguese <https://translate.koha-community.org/projects/koha-manual/globalpreferences/pt/>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Weblate 5.4-dev\n"
"X-Pootle-Path: /pt/manual22.11/globalpreferences.po\n"
"X-Pootle-Revision: 1\n"

#: ../../source/globalpreferences.rst:6
msgid "System preferences"
msgstr "Preferências de sistema"

#: ../../source/globalpreferences.rst:8
msgid "System preferences control the way your Koha system works in general. Set these preferences before anything else in Koha."
msgstr "As preferências de sistema controlam a forma como o sistema Koha vai funcionar. Defina estas preferências antes de começar a explorar o Koha."

#: ../../source/globalpreferences.rst:11
msgid "*Get there:* More > Administration > System preferences"
msgstr "*Acesso:* Mais > Administração > Preferências de sistema"

#: ../../source/globalpreferences.rst:15
msgid "Only staff with the :ref:`manage\\_sysprefs permission <permission-manage-sysprefs-label>` (or the :ref:`superlibrarian permission <permission-superlibrarian-label>`) will have access to this section."
msgstr ""

#: ../../source/globalpreferences.rst:20
msgid "System preferences can be searched (using any part of the preference name or description) using the search box on the 'Administration' page"
msgstr "As preferências de sistema podem ser pesquisas (usando qualquer parte do nome ou da descrição da preferência) na caixa de pesquisa da página principal da 'Administração'"

#: ../../source/globalpreferences.rst:23
msgid "|prefsearch|"
msgstr "|prefsearch|"

#: ../../source/globalpreferences.rst:25
msgid "or the search box at the top of the administration module or system preferences pages."
msgstr "ou na caixa de pesquisa no topo do módulo de administração ou das páginas das preferências de sistema."

#: ../../source/globalpreferences.rst:28
msgid "|searchbar-admin|"
msgstr "|searchbar-admin|"

#: ../../source/globalpreferences.rst:30
msgid "The bookmark icon next to a system preference can be used to create a URL that links directly to the specific system preference, narrowing down your view to only this and related terms. The bookmarked URL can be helpful when you need to point people to a specific system preference and do not want to send the general link."
msgstr ""

#: ../../source/globalpreferences.rst:32
msgid "When editing system preferences a *(modified)* flag will appear next to elements that were changed until you click the 'Save all ... preferences' button."
msgstr "Ao editar as preferências de sistema, a marca *(modificado)* vai aparecer junto aos elementos que foram modificados até que clique no botão 'Guardar todas as preferências de ...'."

#: ../../source/globalpreferences.rst:35
msgid "|saveallprefs|"
msgstr "|saveallprefs|"

#: ../../source/globalpreferences.rst:37
msgid "After saving your preferences you'll get a confirmation message telling you what preferences were saved."
msgstr "Após guardar as preferências de sistema, será mostrada uma mensagem de confirmação a identificar quais as preferências que foram guardadas."

#: ../../source/globalpreferences.rst:40
msgid "|saveconfirmation|"
msgstr "|saveconfirmation|"

#: ../../source/globalpreferences.rst:42
msgid "Each section of system preferences is sorted alphabetically by default. Clicking the small 'up' arrow to the right of the word 'Preference' in the header column will invert the sorting."
msgstr "Cada secção das preferências de sistema está ordenada alfabeticamente por omissão. Ao clicar na pequena seta 'para cima' à direita da palavra 'Preferência' no cabeçalho da tabela vai inverter a ordenação."

#: ../../source/globalpreferences.rst:46
msgid "|sortprefs|"
msgstr "|sortprefs|"

#: ../../source/globalpreferences.rst:48
msgid "If the system preference refers to monetary values ( :ref:`maxoutstanding <maxoutstanding-label>`, for example) the currency displayed will be the default one set in the :ref:`currencies and exchange rates <currencies-and-exchange-rates-label>` administration section."
msgstr "Se a preferência de sistema for referente a valores monetários ( :ref:`maxoutstanding <maxoutstanding-label>`, por exemplo), a moeda mostrada será a que se encontra definida como omissão na secção :ref:`moedas e taxas de câmbio <currencies-and-exchange-rates-label>`da administração."

#: ../../source/globalpreferences.rst:56
msgid "For library systems with unique URLs for each site the system preference can be overridden by editing your koha-http.conf file. This has to be done by a system administrator or someone with access to your system files. For example, if all libraries but one want to have search terms highlighted in results, set the :ref:`OpacHighlightedWords <opachighlightedwords-and-nothighlightedwords-label>` system preference to 'Highlight', then edit the koha-http.conf for the library that wants this turned off by adding :code:`SetEnv OVERRIDE\\_SYSPREF\\_OpacHighlightedWords \"0\"`. After restarting the web server, that one library will no longer see highlighted terms. Consult with your system administrator for more information."
msgstr "Nos sistemas onde existam URLs distintos para cada site das bibliotecas as preferências de sistema podem ser sobrepostas editando o ficheiro de configuração koha-http.conf. Este processo tem que ser feito por um administrador ou por alguém com acesso ao sistema de ficheiros. Por exemplo, se todas as bibliotecas, excepto uma, desejam ter os termos de pesquisa destacados, defina a preferência de sistema ref:`OpacHighlightedWords <opachighlightedwords-and-nothighlightedwords-label>`como 'Destacar' e edite o ficheiro koha-http.conf da biblioteca onde a funcionalidade deve estar desligada adicionando a linha :code:`SetEnv OVERRIDE\\_SYSPREF\\_OpacHighlightedWords \"0\"`. Após reiniciar o servidor web, os termos de pesquisa já não vão aparecer destacados no site dessa biblioteca. Consulte o administrador dos sistema para mais informações sobre este processo."

#: ../../source/images.rst:878
msgid "System preferences search box in the main page of the administration module"
msgstr "Caixa de pesquisa de preferências de sistema na página principal do módulo de administração"

#: ../../source/images.rst:887
msgid "Screenshot of the Features system preferences for the Accounting section. There is a (modified) flag next to AutoCreditNumber and a button at the top of the table 'Save all Accounting preferences'"
msgstr "Imagem das preferências de sistema Funcionalidades da secção de Contabilidade. Existe a marca (modificado) junto à preferência AutoCreditNumber e o botão no topo da tabela para 'Guardar todas as preferências de contabilidade'"

#: ../../source/images.rst:890
msgid "Screenshot of the Features system preferences for the Accounting section. There is an overlaid message reading 'Saved preference AutoCreditNumber'."
msgstr "Imagem das preferências de sistema Funcionalidades da secção de Contabilidade. Existe uma mensagem a identificar 'Preferência AutoCreditNumber guardada'."

#: ../../source/images.rst:899
msgid "Screenshot of the Features system preferences for the Accounting section. Preferences are in reverse alphabetical order."
msgstr "Imagem das preferências de sistema Funcionalidades da secção de Contabilidade. Preferências por ordem alfabética invertida."

#: ../../source/images.rst:3484
msgid "Menu and search bar at the top of the page in the administration module, the system preferences search option is selected by default, other options are checkout and search catalog"
msgstr "Menu e caixa de pesquisa no topo das páginas do módulo de administração, a opção de pesquisa nas preferências de sistema está selecionada por omissão, as outras opções são empréstimo ou pesquisa no catálogo"
