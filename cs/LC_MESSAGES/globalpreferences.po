# Compendium of cs.
msgid ""
msgstr ""
"Project-Id-Version: compendium-cs\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2025-02-04 01:00+0000\n"
"PO-Revision-Date: 2021-05-25 20:24-0300\n"
"Language-Team: Koha Translation Team \n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-publican; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../source/globalpreferences.rst:6
#, fuzzy
msgid "System preferences"
msgstr "Společná nastavení systému"

#: ../../source/globalpreferences.rst:8
#, fuzzy
msgid "System preferences control the way your Koha system works in general. Set these preferences before anything else in Koha."
msgstr "Ve společných nastaveních systému si můžete navolit obecná nastavení toho, jak celá Koha bude fungovat. Je nutné je nastavit před tím než začnete se systémem Koha dále pracovat."

#: ../../source/globalpreferences.rst:11
#, fuzzy
msgid "*Get there:* More > Administration > System preferences"
msgstr "*Naleznete zde*: Více > Administrace > Společná nastavení systému"

#: ../../source/globalpreferences.rst:15
msgid "Only staff with the :ref:`manage\\_sysprefs permission <permission-manage-sysprefs-label>` (or the :ref:`superlibrarian permission <permission-superlibrarian-label>`) will have access to this section."
msgstr ""

#: ../../source/globalpreferences.rst:20
#, fuzzy
msgid "System preferences can be searched (using any part of the preference name or description) using the search box on the 'Administration' page"
msgstr "Systémová nastavení lze vyhledávat (jakoukoliv část názvu referencí nebo popisu) pomocí vyhledávacího pole na stránce \"Administrace\" nebo pomocí vyhledávacího pole v horní části každé stránky \"Předvolby systému\"."

#: ../../source/globalpreferences.rst:23
msgid "|prefsearch|"
msgstr ""

#: ../../source/globalpreferences.rst:25
#, fuzzy
msgid "or the search box at the top of the administration module or system preferences pages."
msgstr "*Naleznete zde*: Více > Administrace > Společná nastavení systému"

#: ../../source/globalpreferences.rst:28
msgid "|searchbar-admin|"
msgstr ""

#: ../../source/globalpreferences.rst:30
msgid "The bookmark icon next to a system preference can be used to create a URL that links directly to the specific system preference, narrowing down your view to only this and related terms. The bookmarked URL can be helpful when you need to point people to a specific system preference and do not want to send the general link."
msgstr ""

#: ../../source/globalpreferences.rst:32
#, fuzzy
msgid "When editing system preferences a *(modified)* flag will appear next to elements that were changed until you click the 'Save all ... preferences' button."
msgstr "Když upravujete systémová nastavení, objeví se poznámka ^(změněno)^ u položky kterou měníte, až do chvíle než stisknete tlačítko 'Uložit nastavení':"

#: ../../source/globalpreferences.rst:35
msgid "|saveallprefs|"
msgstr ""

#: ../../source/globalpreferences.rst:37
#, fuzzy
msgid "After saving your preferences you'll get a confirmation message telling you what preferences were saved."
msgstr "Po uložení systémových nastavení se objeví potvrzení, která systémová nastavení byla uložena:"

#: ../../source/globalpreferences.rst:40
msgid "|saveconfirmation|"
msgstr ""

#: ../../source/globalpreferences.rst:42
#, fuzzy
msgid "Each section of system preferences is sorted alphabetically by default. Clicking the small 'up' arrow to the right of the word 'Preference' in the header column will invert the sorting."
msgstr "Každá sekce předvoleb může být řazena abecedně kliknutím na malou šipku napravo od slova \"Předvolby\" v záhlaví"

#: ../../source/globalpreferences.rst:46
msgid "|sortprefs|"
msgstr ""

#: ../../source/globalpreferences.rst:48
msgid "If the system preference refers to monetary values ( :ref:`maxoutstanding <maxoutstanding-label>`, for example) the currency displayed will be the default one set in the :ref:`currencies and exchange rates <currencies-and-exchange-rates-label>` administration section."
msgstr ""

#: ../../source/globalpreferences.rst:56
msgid "For library systems with unique URLs for each site the system preference can be overridden by editing your koha-http.conf file. This has to be done by a system administrator or someone with access to your system files. For example, if all libraries but one want to have search terms highlighted in results, set the :ref:`OpacHighlightedWords <opachighlightedwords-and-nothighlightedwords-label>` system preference to 'Highlight', then edit the koha-http.conf for the library that wants this turned off by adding :code:`SetEnv OVERRIDE\\_SYSPREF\\_OpacHighlightedWords \"0\"`. After restarting the web server, that one library will no longer see highlighted terms. Consult with your system administrator for more information."
msgstr ""

#: ../../source/images.rst:878
#, fuzzy
msgid "System preferences search box in the main page of the administration module"
msgstr "*Naleznete zde*: Více > Administrace > Společná nastavení systému"

#: ../../source/images.rst:887
msgid "Screenshot of the Features system preferences for the Accounting section. There is a (modified) flag next to AutoCreditNumber and a button at the top of the table 'Save all Accounting preferences'"
msgstr ""

#: ../../source/images.rst:890
msgid "Screenshot of the Features system preferences for the Accounting section. There is an overlaid message reading 'Saved preference AutoCreditNumber'."
msgstr ""

#: ../../source/images.rst:899
msgid "Screenshot of the Features system preferences for the Accounting section. Preferences are in reverse alphabetical order."
msgstr ""

#: ../../source/images.rst:3484
msgid "Menu and search bar at the top of the page in the administration module, the system preferences search option is selected by default, other options are checkout and search catalog"
msgstr ""
