# Swedish translations for Koha Manual package.
# Copyright (C) 2020, Koha Community
# This file is distributed under the same license as the Koha Manual package.
# Automatically generated, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: Koha Manual 22.11\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-12-12 01:03+0000\n"
"PO-Revision-Date: 2025-01-10 01:00+0000\n"
"Last-Translator: plumflower <jeff_lindqvist@hotmail.com>\n"
"Language-Team: Swedish <https://translate.koha-community.org/projects/koha-manual/accountspreferences/sv/>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.6.2\n"
"X-Pootle-Path: /sv/manual22.11/accountspreferences.po\n"
"X-Pootle-Revision: 1\n"

#: ../../source/accountspreferences.rst:6
msgid "Accounting"
msgstr ""

#: ../../source/accountspreferences.rst:8
msgid "*Get there:* More > Administration > System preferences > Acquisitions"
msgstr "*Sökväg:* Mer > Administration > Systeminställningar > Inköpsparametrar"

#: ../../source/accountspreferences.rst:14
msgid "Features"
msgstr ""

#: ../../source/accountspreferences.rst:19
msgid "AutoCreditNumber"
msgstr ""

#: ../../source/accountspreferences.rst:21
msgid "Asks: \\_\\_\\_"
msgstr ""

#: ../../source/accountspreferences.rst:23
msgid "Default: Do not automatically generate credit numbers"
msgstr ""

#: ../../source/accountspreferences.rst:25
#: ../../source/accountspreferences.rst:56
#: ../../source/accountspreferences.rst:80
#: ../../source/accountspreferences.rst:110
#: ../../source/accountspreferences.rst:135
#: ../../source/accountspreferences.rst:156
#: ../../source/accountspreferences.rst:177
#: ../../source/accountspreferences.rst:202
msgid "Values:"
msgstr "Värden:"

#: ../../source/accountspreferences.rst:27
msgid "Do not automatically generate credit numbers"
msgstr ""

#: ../../source/accountspreferences.rst:29
msgid "Automatically generate credit numbers in the form <year>-0001"
msgstr ""

#: ../../source/accountspreferences.rst:31
msgid "Automatically generate credit numbers in the form <branchcode>yyyymm0001"
msgstr ""

#: ../../source/accountspreferences.rst:33
msgid "Automatically generate credit numbers in the form 1, 2, 3"
msgstr ""

#: ../../source/accountspreferences.rst:35
#: ../../source/accountspreferences.rst:62
#: ../../source/accountspreferences.rst:86
#: ../../source/accountspreferences.rst:116
#: ../../source/accountspreferences.rst:141
#: ../../source/accountspreferences.rst:162
#: ../../source/accountspreferences.rst:183
#: ../../source/accountspreferences.rst:208
msgid "Description:"
msgstr "Beskrivning:"

#: ../../source/accountspreferences.rst:37
msgid "For auditing purposes, it might be necessary to attach a unique number to each credit (payments, writeoffs, refunds, etc.). This system preference allows you to choose the format of this unique number."
msgstr ""

#: ../../source/accountspreferences.rst:41
#: ../../source/accountspreferences.rst:66
msgid "**Important**"
msgstr "**Viktigt**"

#: ../../source/accountspreferences.rst:43
msgid "Automatic generation also has to be enabled for each :ref:`credit type <credit-types-label>`."
msgstr ""

#: ../../source/accountspreferences.rst:49
msgid "EnablePointOfSale"
msgstr ""

#: ../../source/accountspreferences.rst:51
msgid "Asks: \\_\\_\\_ the point of sale feature to allow anonymous transactions with the accounting system."
msgstr ""

#: ../../source/accountspreferences.rst:54
msgid "Default: Disable"
msgstr "Standard: Avaktivera"

#: ../../source/accountspreferences.rst:58
msgid "Disable"
msgstr "Avaktivera"

#: ../../source/accountspreferences.rst:60
msgid "Enable"
msgstr "Aktivera"

#: ../../source/accountspreferences.rst:64
msgid "This system preference controls the :ref:`point of sale module <point-of-sale-label>`."
msgstr ""

#: ../../source/accountspreferences.rst:68
msgid "If you enable this system preference, make sure to enable :ref:`UseCashRegisters <UseCashRegisters-label>` as well."
msgstr ""

#: ../../source/accountspreferences.rst:74
msgid "RequireCashRegister"
msgstr ""

#: ../../source/accountspreferences.rst:76
msgid "Asks: When collecting payment, \\_\\_\\_."
msgstr ""

#: ../../source/accountspreferences.rst:78
msgid "Default: require a cash register only when the CASH payment type is selected"
msgstr ""

#: ../../source/accountspreferences.rst:82
msgid "require a cash register only when the CASH payment type is selected"
msgstr ""

#: ../../source/accountspreferences.rst:84
msgid "always require a cash register"
msgstr ""

#: ../../source/accountspreferences.rst:88
msgid "This system preference is only used if the :ref:`UseCashRegisters <usecashregisters-label>` system preference is enabled."
msgstr ""

#: ../../source/accountspreferences.rst:92
msgid "This system preference is used to determine if selecting a cash register is required when :ref:`paying charges <pay-and-writeoff-fines-label>` in a patron's account."
msgstr ""

#: ../../source/accountspreferences.rst:96
msgid "When using :ref:`UseCashRegisters <usecashregisters-label>`, selecting a cash register will always be required when the CASH payment type is chosen. This system preference determines if selecting a cash register is required when choosing another payment type."
msgstr ""

#: ../../source/accountspreferences.rst:104
msgid "UseCashRegisters"
msgstr ""

#: ../../source/accountspreferences.rst:106
msgid "Asks: \\_\\_\\_ cash registers with the accounting system to track payments."
msgstr ""

#: ../../source/accountspreferences.rst:108
msgid "Default: Don't use"
msgstr ""

#: ../../source/accountspreferences.rst:112
msgid "Don't use"
msgstr ""

#: ../../source/accountspreferences.rst:114
msgid "Use"
msgstr ""

#: ../../source/accountspreferences.rst:118
msgid "This preference enables the :ref:`cash registers <cashregisters-label>` feature in the administration module."
msgstr ""

#: ../../source/accountspreferences.rst:124
msgid "Policy"
msgstr ""

#: ../../source/accountspreferences.rst:129
msgid "AccountAutoReconcile"
msgstr ""

#: ../../source/accountspreferences.rst:131
msgid "Asks: \\_\\_\\_ reconcile patron balances automatically on each transaction adding debits or credits."
msgstr ""

#: ../../source/accountspreferences.rst:133
msgid "Default: Do not"
msgstr ""

#: ../../source/accountspreferences.rst:137
#: ../../source/accountspreferences.rst:158
#: ../../source/accountspreferences.rst:206
msgid "Do"
msgstr ""

#: ../../source/accountspreferences.rst:139
msgid "Do not"
msgstr ""

#: ../../source/accountspreferences.rst:143
msgid "This preference controls whether or not credits are automatically used to to reduce the owed amounts in a patron's account."
msgstr ""

#: ../../source/accountspreferences.rst:149
msgid "FinePaymentAutoPopup"
msgstr ""

#: ../../source/accountspreferences.rst:151
msgid "Asks: \\_\\_\\_ automatically display a print dialog for a payment receipt when making a payment."
msgstr ""

#: ../../source/accountspreferences.rst:154
#: ../../source/accountspreferences.rst:200
msgid "Default: Don't"
msgstr ""

#: ../../source/accountspreferences.rst:160
#: ../../source/accountspreferences.rst:204
msgid "Don't"
msgstr ""

#: ../../source/accountspreferences.rst:164
msgid "If activated, when :ref:`making a payment <pay-and-writeoff-fines-label>` in a patron's account, a printing popup will be displayed automatically instead of having to click on the 'print' button."
msgstr ""

#: ../../source/accountspreferences.rst:171
msgid "RequirePaymentType"
msgstr ""

#: ../../source/accountspreferences.rst:173
msgid "Asks: \\_\\_\\_ staff to select a payment type when a payment is made."
msgstr ""

#: ../../source/accountspreferences.rst:175
msgid "Default: Do not require"
msgstr ""

#: ../../source/accountspreferences.rst:179
msgid "Do not require"
msgstr ""

#: ../../source/accountspreferences.rst:181
msgid "Require"
msgstr ""

#: ../../source/accountspreferences.rst:185
msgid "This system preference is only used if the :ref:`UseCashRegisters <usecashregisters-label>` system preference disabled."
msgstr ""

#: ../../source/accountspreferences.rst:189
msgid "This system preference is used to make the 'Payment type' field mandatory when :ref:`paying charges <pay-and-writeoff-fines-label>` in a patron's account."
msgstr ""

#: ../../source/accountspreferences.rst:196
msgid "RoundFinesAtPayment"
msgstr ""

#: ../../source/accountspreferences.rst:198
msgid "Asks: \\_\\_\\_ round fines to the nearest cent when collecting payments."
msgstr ""

#: ../../source/accountspreferences.rst:210
msgid "Enabling this preference allows paying fines of partial cents which may not be visible in the interface."
msgstr ""
