# Compendium of sv.
msgid ""
msgstr ""
"Project-Id-Version: Koha Manual 22.11\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-06-15 01:13+0000\n"
"PO-Revision-Date: 2025-01-14 01:00+0000\n"
"Last-Translator: plumflower <jeff_lindqvist@hotmail.com>\n"
"Language-Team: Swedish <https://translate.koha-community.org/projects/koha-manual/ermpreferences/sv/>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.6.2\n"
"X-Pootle-Path: /sv/manual22.11/ermpreferences.po\n"
"X-Pootle-Revision: 1\n"

#: ../../source/ermpreferences.rst:6
msgid "E-Resource management"
msgstr "E-resurshantering"

#: ../../source/ermpreferences.rst:8
msgid "The E-Resource management tab in Koha will give you settings to enable and configure the :ref:`E-Resource management (ERM) module <erm-label>`."
msgstr "Fliken E-resurshantering i Koha ger dig inställningar för att aktivera och konfigurera :ref:`E-resurshanterings-modulen (ERM) <erm-label>`."

#: ../../source/ermpreferences.rst:11
msgid "Get there: More > Administration > System Preferences > E-Resource management"
msgstr "Sökväg: Mer > Administration > Systeminställningar > E-resurshantering"

#: ../../source/ermpreferences.rst:16
msgid "Interface"
msgstr "Gränssnitt"

#: ../../source/ermpreferences.rst:21
msgid "ERMModule"
msgstr "ERMModule"

#: ../../source/ermpreferences.rst:23
msgid "Asks: \\_\\_\\_ the e-resource management module"
msgstr "Frågar: \\_\\_\\_ e-resurshanteringsmodulen"

#: ../../source/ermpreferences.rst:25 ../../source/ermpreferences.rst:88
msgid "Values:"
msgstr "Värden:"

#: ../../source/ermpreferences.rst:27
msgid "Disable"
msgstr "Avaktivera"

#: ../../source/ermpreferences.rst:29
msgid "Enable"
msgstr "Aktivera"

#: ../../source/ermpreferences.rst:31
msgid "Default: Disable"
msgstr "Standard: Avaktivera"

#: ../../source/ermpreferences.rst:33 ../../source/ermpreferences.rst:52
#: ../../source/ermpreferences.rst:70 ../../source/ermpreferences.rst:98
msgid "Description:"
msgstr "Beskrivning:"

#: ../../source/ermpreferences.rst:35
msgid "This preference controls whether or not staff can use the :ref:`E-Resource management (ERM) module <erm-label>`."
msgstr "Denna inställning styr huruvida personal kan använda modulen :ref:`E-Resource Management (ERM) <erm-label>`."

#: ../../source/ermpreferences.rst:37
msgid "This is the main switch for the E-Resource management functionality."
msgstr ""

#: ../../source/ermpreferences.rst:41
msgid "To use the module, staff also need to have the :ref:`erm permission <permission-erm-label>` (or the :ref:`superlibrarian permission <permission-superlibrarian-label>`)."
msgstr ""

#: ../../source/ermpreferences.rst:48
msgid "ERMProviderEbscoApiKey"
msgstr ""

#: ../../source/ermpreferences.rst:50
msgid "Asks: API key for EBSCO HoldingsIQ \\_\\_\\_"
msgstr ""

#: ../../source/ermpreferences.rst:55
msgid "knowledge base using their `HoldingsIQ <https://developer.ebsco.com/knowledge-services/holdingsiq>`__ tool. The library will need to ask EBSCO for both an API key and a Customer ID (see :ref:`ERMProviderEbscoCustomerID <erm-ermproviderebscocustomerid-label>` below). Once correctly configured the EBSCO knowledge base will be accessible via the :ref:`eHoldings <erm-eholdings-label>` menu option in E-Resource management."
msgstr ""

#: ../../source/ermpreferences.rst:59 ../../source/ermpreferences.rst:77
msgid "This preference allows libraries to integrate with the EBSCO global"
msgstr ""

#: ../../source/ermpreferences.rst:61 ../../source/ermpreferences.rst:79
msgid "If this preference is left empty, you will be unable to connect the EBSCO knowledge base."
msgstr ""

#: ../../source/ermpreferences.rst:66
msgid "ERMProviderEbscoCustomerID"
msgstr ""

#: ../../source/ermpreferences.rst:68
msgid "Asks: Customer ID for EBSCO HoldingsIQ \\_\\_\\_"
msgstr ""

#: ../../source/ermpreferences.rst:73
msgid "knowledge base using their `HoldingsIQ <https://developer.ebsco.com/knowledge-services/holdingsiq>`__ tool. The library will need to ask EBSCO for both an API key and a Customer ID (see :ref:`ERMProviderEbscoApiKey <erm-ermproviderebscoapikey-label>` above). Once correctly configured the EBSCO knowledge base will be accessible via the :ref:`eHoldings <erm-eholdings-label>` menu option in E-Resource management."
msgstr ""

#: ../../source/ermpreferences.rst:84
msgid "ERMProviders"
msgstr ""

#: ../../source/ermpreferences.rst:86
msgid "Asks: Providers for the e-resource management module \\_\\_\\_"
msgstr ""

#: ../../source/ermpreferences.rst:90
msgid "Select all"
msgstr ""

#: ../../source/ermpreferences.rst:92
msgid "EBSCO"
msgstr ""

#: ../../source/ermpreferences.rst:94
msgid "Local"
msgstr ""

#: ../../source/ermpreferences.rst:96
msgid "Default: Local"
msgstr ""

#: ../../source/ermpreferences.rst:101
msgid "This preference controls whether to show one or more sources of"
msgstr ""

#: ../../source/ermpreferences.rst:101
msgid "eHoldings in the :ref:`eHoldings <erm-eholdings-label>` menu option."
msgstr ""

#: ../../source/ermpreferences.rst:103
msgid "Selecting \"EBSCO\" allows integration with the EBSCO global knowledge base."
msgstr ""

#: ../../source/ermpreferences.rst:105
msgid "Selecting \"Local\" provides the ability to manually create eHoldings records for local sources."
msgstr ""
