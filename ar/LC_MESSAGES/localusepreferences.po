# Compendium of ar.
msgid ""
msgstr ""
"Project-Id-Version: Koha Manual 22.11\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-12-12 01:03+0000\n"
"PO-Revision-Date: 2021-05-25 20:25-0300\n"
"Language-Team: Koha Translation Team \n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-publican; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Pootle-Path: /ar/manual22.11/localusepreferences.po\n"
"X-Pootle-Revision: 1\n"

#: ../../source/localusepreferences.rst:6
#, fuzzy
msgid "Local use"
msgstr "استخدام محلي"

#: ../../source/localusepreferences.rst:8
#, fuzzy
msgid "These preferences are defined locally."
msgstr "مفضلات النظام غير معرفة في تبويبات أخرى."

#: ../../source/localusepreferences.rst:10
#, fuzzy
msgid "*Get there:* More > Administration > System preferences > Local Use"
msgstr "للوصول: المزيد > الإدارة > تفضيلات النظام العامة"

#: ../../source/localusepreferences.rst:13
msgid "**Note**"
msgstr "**ملاحظة**"

#: ../../source/localusepreferences.rst:15
msgid "Sometimes preferences which are either new or outdated will appear in this tab, if you didn't add any preferences to this tab then it's best to ignore preferences listed here."
msgstr "بعض الأحيان سواء كانت التفضيلات جديدة أو قديمة سوف تظهر في هذا التبويب، إذا لم تضف أية تفضيلات لهذا التبويب من الأفضل تجاهل التفضيلات المذكورة هنا."

#: ../../source/localusepreferences.rst:22
#, fuzzy
msgid "INTRAdidyoumean"
msgstr "هل تقصد:"

#: ../../source/localusepreferences.rst:24
#: ../../source/localusepreferences.rst:34
#: ../../source/localusepreferences.rst:59
#: ../../source/localusepreferences.rst:69
#: ../../source/localusepreferences.rst:79
#, fuzzy
msgid "Default: blank"
msgstr "الإفتراضي: لا"

#: ../../source/localusepreferences.rst:26
msgid "Asks: Did you mean? configuration for the Intranet. Do not change, as this is controlled by /cgi-bin/koha/admin/didyoumean.pl."
msgstr ""

#: ../../source/localusepreferences.rst:32
#, fuzzy
msgid "OPACdidyoumean"
msgstr "هل تقصد:"

#: ../../source/localusepreferences.rst:36
msgid "Asks: Did you mean? configuration for the OPAC. Do not change, as this is controlled by /cgi-bin/koha/admin/didyoumean.pl."
msgstr ""

#: ../../source/localusepreferences.rst:42
#, fuzzy
msgid "printcirculationships"
msgstr "تاريخ إعارة المستفيد"

#: ../../source/localusepreferences.rst:44
#, fuzzy
msgid "Default: ON"
msgstr "الافتراضي: OSt"

#: ../../source/localusepreferences.rst:46
msgid "Asks: If ON, enable printing circulation receipts"
msgstr ""

#: ../../source/localusepreferences.rst:48
msgid "Values:"
msgstr "القيم:"

#: ../../source/localusepreferences.rst:50
msgid "ON"
msgstr "فتح"

#: ../../source/localusepreferences.rst:52
msgid "OFF"
msgstr "إغلاق"

#: ../../source/localusepreferences.rst:57
msgid "UsageStatsID"
msgstr ""

#: ../../source/localusepreferences.rst:61
#: ../../source/localusepreferences.rst:71
msgid "Asks: This preference is part of Koha but it should not be deleted or updated manually."
msgstr ""

#: ../../source/localusepreferences.rst:67
msgid "UsageStatsLastUpdateTime"
msgstr ""

#: ../../source/localusepreferences.rst:77
msgid "UsageStatsPublicID"
msgstr ""

#: ../../source/localusepreferences.rst:81
msgid "Asks: Public ID for Hea website"
msgstr ""

#: ../../source/localusepreferences.rst:86
msgid "Version"
msgstr "إصدار"

#: ../../source/localusepreferences.rst:88
#, fuzzy
msgid "Default: automatically generated"
msgstr "الأفتراضي: تلقائياً"

#: ../../source/localusepreferences.rst:90
msgid "Asks: The Koha database version. WARNING: Do not change this value manually. It is maintained by the webinstaller"
msgstr ""
