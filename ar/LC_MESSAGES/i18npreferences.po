# Compendium of ar.
msgid ""
msgstr ""
"Project-Id-Version: Koha Manual 22.11\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2025-02-06 01:00+0000\n"
"PO-Revision-Date: 2021-05-25 20:25-0300\n"
"Language-Team: Koha Translation Team \n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-publican; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Pootle-Path: /ar/manual22.11/i18npreferences.po\n"
"X-Pootle-Revision: 1\n"

#: ../../source/i18npreferences.rst:6
msgid "I18N/L10N"
msgstr "اللغات العالمية"

#: ../../source/i18npreferences.rst:8
#, fuzzy
msgid "These preferences control your Internationalization and Localization settings."
msgstr "يحتوي على تفضيلات مرتبطة بالتدويل والتوطين مثل صيغ التاريخ واللغات."

#: ../../source/i18npreferences.rst:11
#, fuzzy
msgid "*Get there:* More > Administration > System preferences > I18N/L10N"
msgstr "للوصول: المزيد > الإدارة > تفضيلات النظام العامة"

#: ../../source/i18npreferences.rst:17
#, fuzzy
msgid "AddressFormat"
msgstr "عنوان"

# I18N/L10N
#: ../../source/i18npreferences.rst:19
msgid "Asks: Format postal addresses using \\_\\_\\_"
msgstr "الصيغة: تنسيق العناوين البريدية باستخدام \\_\\_\\_"

#: ../../source/i18npreferences.rst:21 ../../source/i18npreferences.rst:65
#: ../../source/i18npreferences.rst:98 ../../source/i18npreferences.rst:131
#: ../../source/i18npreferences.rst:172 ../../source/i18npreferences.rst:215
#: ../../source/i18npreferences.rst:244 ../../source/i18npreferences.rst:288
#: ../../source/i18npreferences.rst:306
msgid "Values:"
msgstr "القيم:"

# I18N/L10N
#: ../../source/i18npreferences.rst:23
#, fuzzy
msgid "German style ([Address] [Street number] - [Zip/Postal Code] [City] - [Country])"
msgstr "German style ([Address] [Street number] - [ZIP/Postal Code] [City] - [Country])"

# I18N/L10N
#: ../../source/i18npreferences.rst:26
#, fuzzy
msgid "French style ([Street number] [Address] - [ZIP/Postal Code] [City] - [Country])"
msgstr "النمط الفرنسي ([رقم الشارع] [العنوان] - [الرمز البريدي] [المدينة] - [البلد])"

# I18N/L10N
#: ../../source/i18npreferences.rst:29
#, fuzzy
msgid "US style ([Street number], [Address] - [City], [Zip/Postal Code], [Country])"
msgstr "US style ([Street number], [Address] - [City], [ZIP/Postal Code], [Country])"

# I18N/L10N
#: ../../source/i18npreferences.rst:32
#, fuzzy
msgid "Default: US style ([Street number], [Address] - [City], [Zip/Postal Code], [Country])"
msgstr "US style ([Street number], [Address] - [City], [ZIP/Postal Code], [Country])"

#: ../../source/i18npreferences.rst:35 ../../source/i18npreferences.rst:51
#: ../../source/i18npreferences.rst:83 ../../source/i18npreferences.rst:110
#: ../../source/i18npreferences.rst:142 ../../source/i18npreferences.rst:178
#: ../../source/i18npreferences.rst:223 ../../source/i18npreferences.rst:250
#: ../../source/i18npreferences.rst:314
msgid "Description:"
msgstr "وصف:"

#: ../../source/i18npreferences.rst:37
msgid "This preference will let you control how Koha displays patron addresses given the information entered in the various fields on their record."
msgstr ""

#: ../../source/i18npreferences.rst:44
msgid "alphabet"
msgstr ""

#: ../../source/i18npreferences.rst:46
msgid "Asks: Use the alphabet \\_\\_\\_ for lists of browsable letters. This should be a space separated list of uppercase letters. Hint: Changing collation in the database for the 'surname' column of the 'borrowers' table is helpful to make browsing by last name work in members-home.pl when using an alphabet outside of A-Z."
msgstr ""

#: ../../source/i18npreferences.rst:49
msgid "Default: A B C D E F G H I J K L M N O P Q R S T U V W X Y Z"
msgstr ""

#: ../../source/i18npreferences.rst:53
#, fuzzy
msgid "This preference allows you define your own alphabet for browsing patrons in Koha."
msgstr "وهذا التفضيل تتيح لك تحديد عدد عرض النتائج بشكل افتراضي عند تشغيل البحث على عميل الموظفين."

#: ../../source/i18npreferences.rst:56
msgid "|alphabetpatronbrowse|"
msgstr ""

#: ../../source/i18npreferences.rst:61
msgid "CalendarFirstDayOfWeek"
msgstr "CalendarFirstDayOfWeek"

# I18N/L10N
#: ../../source/i18npreferences.rst:63
msgid "Asks: Use \\_\\_\\_ as the first day of week in the calendar."
msgstr "الصيغة: استخدم \\_\\_\\_ كأول يوم في الأسبوع في الروزنامة."

#: ../../source/i18npreferences.rst:67
msgid "Sunday"
msgstr "الأحد"

#: ../../source/i18npreferences.rst:69
msgid "Monday"
msgstr "الإثنين"

#: ../../source/i18npreferences.rst:71
msgid "Tuesday"
msgstr "الثلاثاء"

#: ../../source/i18npreferences.rst:73
msgid "Wednesday"
msgstr "الاربعاء"

#: ../../source/i18npreferences.rst:75
msgid "Thursday"
msgstr "الخميس"

#: ../../source/i18npreferences.rst:77
msgid "Friday"
msgstr "الجمعة"

#: ../../source/i18npreferences.rst:79
msgid "Saturday"
msgstr "السبت"

#: ../../source/i18npreferences.rst:81
msgid "Default: Sunday"
msgstr "الافتراضي: الأحد"

#: ../../source/i18npreferences.rst:85
#, fuzzy
msgid "Using this preference, you can control what day shows as the first day of the week in the calendar pop ups throughout Koha and on the Calendar tool. If you change this preference and don't see a change in your browser, try clearing your cache since it makes changes to the Javascript on these pages."
msgstr "باستخدام هذا التفضيل يمكنك أن تتحكم باليوم الذي تريده أن يظهر كأول يوم من الأسبوع في الإطارات المنبثقة للتقويم من خلال كوها و أداة التقويم. اذا قمت بتغيير هذا التفضيل ولم ترى التغيير في المتصفح الخاص بك حاول ان تقوم بمسح ذاكرة الكاش لأنه يقوم بتغييرات للجافا على هذه الصفحات."

#: ../../source/i18npreferences.rst:94
msgid "dateformat"
msgstr "تنسيق التاريخ"

#: ../../source/i18npreferences.rst:96
#, fuzzy
msgid "Asks: Format dates like \\_\\_\\_"
msgstr "الصيغة: إنشاء مادة عندما \\_\\_\\_."

#: ../../source/i18npreferences.rst:100
#, fuzzy
msgid "dd.mm.yyyy"
msgstr "dd/mm/yyyy"

#: ../../source/i18npreferences.rst:102
#, fuzzy
msgid "yyyy-mm-dd"
msgstr "yyyy/mm/dd"

#: ../../source/i18npreferences.rst:104
msgid "dd/mm/yyyy"
msgstr "dd/mm/yyyy"

#: ../../source/i18npreferences.rst:106
msgid "mm/dd/yyyy"
msgstr "mm/dd/yyyy"

#: ../../source/i18npreferences.rst:108
msgid "Default: mm/dd/yyyy"
msgstr "الافتراضي: mm/dd/yyyy"

#: ../../source/i18npreferences.rst:112
msgid "This preference controls how the date is displayed."
msgstr ""

#: ../../source/i18npreferences.rst:114
msgid "The options are: mm/dd/yyyy (e.g., 04/24/2010 for 24 April 2010); dd/mm/yyyy (24/04/2010); dd.mm.yyyy (24.04.2010); the ISO (International Organization for Standardization) format yyyy-mm-dd (2010-04-24)."
msgstr ""

#: ../../source/i18npreferences.rst:116
msgid "The ISO format would primarily be used by libraries with locations in multiple nations that may use different date formats, to have a single display type."
msgstr ""

#: ../../source/i18npreferences.rst:122
msgid "language"
msgstr "اللغة"

#: ../../source/i18npreferences.rst:126
msgid "This system preference was replaced by :ref:`StaffInterfaceLanguages <staffinterfacelanguages-label>` from version 24.11 of Koha."
msgstr ""

#: ../../source/i18npreferences.rst:129
msgid "Asks: Enable the following languages on the staff interface"
msgstr "المطلوب: تفعيل اللغات التالية على واجهة الموظفين"

#: ../../source/i18npreferences.rst:133 ../../source/i18npreferences.rst:174
#: ../../source/i18npreferences.rst:246
#, fuzzy
msgid "English (en)"
msgstr "الإنجليزية"

#: ../../source/i18npreferences.rst:135 ../../source/i18npreferences.rst:176
#: ../../source/i18npreferences.rst:248
#, fuzzy
msgid "Default: English (en)"
msgstr "الافتراضي: الانكليزية"

#: ../../source/i18npreferences.rst:139
msgid "To install additional languages, please refer to the Koha Wiki page `Installation of additional languages for OPAC and INTRANET staff client <https://wiki.koha-community.org/wiki/Installation_of_additional_languages_for_OPAC_and_INTRANET_staff_client>`_. Once an additional language is installed, it will show as an option in this preference."
msgstr ""

#: ../../source/i18npreferences.rst:144
msgid "This system preference controls which of the installed languages are enabled in the staff interface. Tick a language and save the system preference to enable that language; untick it and save to disable."
msgstr ""

#: ../../source/i18npreferences.rst:148 ../../source/i18npreferences.rst:264
msgid "If more than one language is enabled, you will see a language selector in the staff interface. Use the :ref:`StaffLangSelectorMode <stafflangselectormode-label>` preference to control where this selector will appear on the page: top, footer or both."
msgstr ""

#: ../../source/i18npreferences.rst:153 ../../source/i18npreferences.rst:269
msgid "If more than one language is enabled, the first one listed in this preference will be the default language of your system. You will also see references to the default when translating notices (if using :ref:`TranslateNotices <translatenotices-label>`) and in :ref:`HTML customizations <html-customizations-label>`, :ref:`News <news-label>` and :ref:`Pages <additional-content-pages-label>`."
msgstr ""

#: ../../source/i18npreferences.rst:159 ../../source/i18npreferences.rst:275
msgid "If more than one language is installed, languages will appear (in the language selector, HTML customizations, etc.) in the same order as they are listed in this preference. You can change the order of the languages using drag and drop: click on a language, hold, move it to its new position, drop it there, then save your changes to the preference."
msgstr ""

#: ../../source/i18npreferences.rst:168
#, fuzzy
msgid "OPACLanguages"
msgstr "اللغات"

#: ../../source/i18npreferences.rst:170
#, fuzzy
msgid "Asks: Enable the following languages on the OPAC \\_\\_\\_"
msgstr "المطلوب: تفعيل اللغات التالية على الأوباك"

#: ../../source/i18npreferences.rst:180
msgid "This system preference controls which of the installed languages are enabled in the OPAC."
msgstr ""

#: ../../source/i18npreferences.rst:185 ../../source/i18npreferences.rst:257
msgid "To install additional languages, please refer to the Koha Wiki page `Installation of additional languages for OPAC and INTRANET staff client <https://wiki.koha-community.org/wiki/Installation_of_additional_languages_for_OPAC_and_INTRANET_staff_client>`_. Once a new language is installed, it will show as an option in this preference."
msgstr ""

#: ../../source/i18npreferences.rst:189 ../../source/i18npreferences.rst:261
msgid "Tick a language and save the system preference to enable that language; untick it and save to disable."
msgstr ""

#: ../../source/i18npreferences.rst:192
msgid "The first enabled language listed in this preference will be the default language of your OPAC."
msgstr ""

#: ../../source/i18npreferences.rst:195
msgid "If more than one language is enabled and :ref:`opaclanguagesdisplay <opaclanguagesdisplay-label>` is set to 'Allow', you will see a language selector on OPAC pages."
msgstr ""

#: ../../source/i18npreferences.rst:198
msgid "Where this selector appears is determined by the :ref:`OpacLangSelectorMode <OpacLangSelectorMode-label>` preference."
msgstr ""

#: ../../source/i18npreferences.rst:201
msgid "The languages will be displayed in the selector in the same order as they are listed in OPACLanguages."
msgstr ""

#: ../../source/i18npreferences.rst:204
msgid "You can change the order of the languages using drag and drop: click on a language, hold, move it to its new position, drop it there, then save your changes to the preference."
msgstr ""

#: ../../source/i18npreferences.rst:211
msgid "opaclanguagesdisplay"
msgstr "opaclanguagesdisplay"

#: ../../source/i18npreferences.rst:213
#, fuzzy
msgid "Asks: \\_\\_\\_ patrons to change the language they see on the OPAC."
msgstr "الصيغة: \\_\\_\\_ للمستفيدين بتحديد لغتهم في الأوباك."

#: ../../source/i18npreferences.rst:217 ../../source/i18npreferences.rst:308
msgid "Don't allow"
msgstr "عدم السماح"

#: ../../source/i18npreferences.rst:219 ../../source/i18npreferences.rst:310
msgid "Allow"
msgstr "السماح"

#: ../../source/i18npreferences.rst:221 ../../source/i18npreferences.rst:312
msgid "Default: Don't allow"
msgstr "الإفتراضي: لا تسمح"

#: ../../source/i18npreferences.rst:225
msgid "When Allow is selected, patrons will see a language selector on the public catalog. Use the :ref:`OpacLangSelectorMode <OpacLangSelectorMode-label>` preference to control where this selector will appear on the page."
msgstr ""

#: ../../source/i18npreferences.rst:228
#, fuzzy
msgid "|opaclanguagesdisplayfooter|"
msgstr "opaclanguagesdisplay"

#: ../../source/i18npreferences.rst:230
msgid "When Don't allow is selected, there is no option for a patron to choose their preferred language on the public catalog."
msgstr ""

#: ../../source/i18npreferences.rst:235
msgid "StaffInterfaceLanguages"
msgstr ""

#: ../../source/i18npreferences.rst:237 ../../source/i18npreferences.rst:322
msgid "Version"
msgstr ""

#: ../../source/i18npreferences.rst:239
msgid "This system preference was first introduced in version 24.11 of Koha. It replaces the :ref:`language <language-label>` system preference."
msgstr ""

#: ../../source/i18npreferences.rst:242
#, fuzzy
msgid "Asks: Enable the following languages on the staff interface: \\_\\_\\_"
msgstr "المطلوب: تفعيل اللغات التالية على واجهة الموظفين"

#: ../../source/i18npreferences.rst:252
msgid "This system preference controls which of the installed languages are enabled in the staff interface."
msgstr ""

#: ../../source/i18npreferences.rst:284
msgid "TimeFormat"
msgstr "النوع / الشكل"

# I18N/L10N
#: ../../source/i18npreferences.rst:286
#, fuzzy
msgid "Asks: Format times in \\_\\_\\_"
msgstr "الصيغة: تنسيق العناوين البريدية باستخدام \\_\\_\\_"

# I18N/L10N
#: ../../source/i18npreferences.rst:290
#, fuzzy
msgid "12 hour format (e.g. \"02:18 PM\")"
msgstr "تنسيق 12 ساعة  (مثال \"02:18 م\" )"

# I18N/L10N
#: ../../source/i18npreferences.rst:292
#, fuzzy
msgid "24 hour format (e.g. \"14:18\")"
msgstr "تنسيق 24 ساعة  ( مثال\"14:18\" )"

# I18N/L10N
#: ../../source/i18npreferences.rst:294
#, fuzzy
msgid "Default: 24 hour format (e.g. \"14:18\")"
msgstr "تنسيق 24 ساعة  ( مثال\"14:18\" )"

#: ../../source/i18npreferences.rst:299
#, fuzzy
msgid "TranslateNotices"
msgstr "ترجمات"

# I18N/L10N
#: ../../source/i18npreferences.rst:301
#, fuzzy
msgid "Asks: \\_\\_\\_ notices to be translated. If set, notices will be translatable from the :ref:`\"Notices and Slips\" interface <adding-notices-and-slips-label>`. The language used to send a notice to a patron will be the one defined for the patron."
msgstr "الصيغة: \\_\\_\\_ بترجمة الإخطارات. عند الضبط، ستكون الإخطارات قابلة للترجمة من واجهة \"الإخطارات والكعوب\". اللغة المستخدمة لإرسال الإخطارات إلى المستفيد ستكون هي اللغة المعرّفة للمستفيد."

#: ../../source/i18npreferences.rst:316
msgid "If set to 'Allow' it is possible to choose a 'Preferred language for notices' when :ref:`creating a new patron account in the staff interface <add-a-new-patron-label>`, or for the patron to do this themselves from their :ref:`messaging options <your-messaging-label>` when logged into their account on the OPAC."
msgstr ""

#: ../../source/i18npreferences.rst:324
msgid "As of Koha version 23.11, the 'Preferred language for notices' can also be set when the patron :ref:`registers online on the OPAC <opac-self-registration-label>`. If needed, you can disable this field using the :ref:`PatronSelfRegistrationBorrowerUnwantedField <PatronSelfRegistrationBorrowerUnwantedField-label>` system preference."
msgstr ""

#: ../../source/images.rst:776
msgid "On the Patrons module homepage, the alphabet is displayed next to \"Browse by last name\"."
msgstr ""

#: ../../source/images.rst:860
msgid "Language selector in the footer of the Opac, with text: \"Languages: English Español Français\"."
msgstr ""
