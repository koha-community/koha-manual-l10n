# Compendium of ar.
msgid ""
msgstr ""
"Project-Id-Version: Koha Manual 22.11\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-12-12 01:03+0000\n"
"PO-Revision-Date: 2021-05-25 20:25-0300\n"
"Language-Team: Koha Translation Team \n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-publican; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Pootle-Path: /ar/manual22.11/accountspreferences.po\n"
"X-Pootle-Revision: 1\n"

#: ../../source/accountspreferences.rst:6
msgid "Accounting"
msgstr "الحسابات"

#: ../../source/accountspreferences.rst:8
#, fuzzy
msgid "*Get there:* More > Administration > System preferences > Acquisitions"
msgstr "للوصول: المزيد > الإدارة > تفضيلات النظام العامة"

#: ../../source/accountspreferences.rst:14
msgid "Features"
msgstr "الميزات"

#: ../../source/accountspreferences.rst:19
msgid "AutoCreditNumber"
msgstr "AutoCreditNumber"

#: ../../source/accountspreferences.rst:21
#, fuzzy
msgid "Asks: \\_\\_\\_"
msgstr "الصيغة: \\_\\_\\_ الإعارة بالدفعة"

# Accounting > Features
#: ../../source/accountspreferences.rst:23
#, fuzzy
msgid "Default: Do not automatically generate credit numbers"
msgstr "عدم توليد أرقام الائتمان تلقائياً"

#: ../../source/accountspreferences.rst:25
#: ../../source/accountspreferences.rst:56
#: ../../source/accountspreferences.rst:80
#: ../../source/accountspreferences.rst:110
#: ../../source/accountspreferences.rst:135
#: ../../source/accountspreferences.rst:156
#: ../../source/accountspreferences.rst:177
#: ../../source/accountspreferences.rst:202
msgid "Values:"
msgstr "القيم:"

#: ../../source/accountspreferences.rst:27
#, fuzzy
msgid "Do not automatically generate credit numbers"
msgstr "تفعيل التوليد التلقائي لرقم الائتمان (انظر"

# Accounting > Features
#: ../../source/accountspreferences.rst:29
#, fuzzy
msgid "Automatically generate credit numbers in the form <year>-0001"
msgstr "توليد أرقام الائتمان تلقائياً بصيغة <year>-0001"

# Accounting > Features
#: ../../source/accountspreferences.rst:31
#, fuzzy
msgid "Automatically generate credit numbers in the form <branchcode>yyyymm0001"
msgstr "توليد أرقام الائتمان تلقائياً بصيغة <branchcode>yyyymm0001"

# Accounting > Features
#: ../../source/accountspreferences.rst:33
#, fuzzy
msgid "Automatically generate credit numbers in the form 1, 2, 3"
msgstr "توليد أرقام الائتمان تلقائياً بصيغة 1، 2، 3"

#: ../../source/accountspreferences.rst:35
#: ../../source/accountspreferences.rst:62
#: ../../source/accountspreferences.rst:86
#: ../../source/accountspreferences.rst:116
#: ../../source/accountspreferences.rst:141
#: ../../source/accountspreferences.rst:162
#: ../../source/accountspreferences.rst:183
#: ../../source/accountspreferences.rst:208
msgid "Description:"
msgstr "وصف:"

#: ../../source/accountspreferences.rst:37
msgid "For auditing purposes, it might be necessary to attach a unique number to each credit (payments, writeoffs, refunds, etc.). This system preference allows you to choose the format of this unique number."
msgstr ""

#: ../../source/accountspreferences.rst:41
#: ../../source/accountspreferences.rst:66
msgid "**Important**"
msgstr "**هام**"

# Accounting > Features
#: ../../source/accountspreferences.rst:43
#, fuzzy
msgid "Automatic generation also has to be enabled for each :ref:`credit type <credit-types-label>`."
msgstr "يجب أيضاً تفعيل التوليد التلقائي لكل نوع ائتمان (<a href=\"/cgi-bin/koha/admin/credit_types.pl\">تهيئة أنواع الائتمان</a>)"

#: ../../source/accountspreferences.rst:49
#, fuzzy
msgid "EnablePointOfSale"
msgstr "نقطة البيع"

# Accounting > Features
#: ../../source/accountspreferences.rst:51
#, fuzzy
msgid "Asks: \\_\\_\\_ the point of sale feature to allow anonymous transactions with the accounting system."
msgstr "  خاصية نقطة البيع للسماح بالتعاملات المالية بدون تعريف الهوية مع نظام المحاسبة. (يتطلب UseCashRegisters)"

#: ../../source/accountspreferences.rst:54
msgid "Default: Disable"
msgstr "الافتراضي: تعطيل"

#: ../../source/accountspreferences.rst:58
msgid "Disable"
msgstr "تعطيل"

#: ../../source/accountspreferences.rst:60
msgid "Enable"
msgstr "فعّل"

#: ../../source/accountspreferences.rst:64
msgid "This system preference controls the :ref:`point of sale module <point-of-sale-label>`."
msgstr ""

#: ../../source/accountspreferences.rst:68
msgid "If you enable this system preference, make sure to enable :ref:`UseCashRegisters <UseCashRegisters-label>` as well."
msgstr ""

#: ../../source/accountspreferences.rst:74
#, fuzzy
msgid "RequireCashRegister"
msgstr "UseCashRegisters"

#: ../../source/accountspreferences.rst:76
msgid "Asks: When collecting payment, \\_\\_\\_."
msgstr ""

#: ../../source/accountspreferences.rst:78
msgid "Default: require a cash register only when the CASH payment type is selected"
msgstr ""

#: ../../source/accountspreferences.rst:82
msgid "require a cash register only when the CASH payment type is selected"
msgstr ""

#: ../../source/accountspreferences.rst:84
msgid "always require a cash register"
msgstr ""

#: ../../source/accountspreferences.rst:88
msgid "This system preference is only used if the :ref:`UseCashRegisters <usecashregisters-label>` system preference is enabled."
msgstr ""

#: ../../source/accountspreferences.rst:92
msgid "This system preference is used to determine if selecting a cash register is required when :ref:`paying charges <pay-and-writeoff-fines-label>` in a patron's account."
msgstr ""

#: ../../source/accountspreferences.rst:96
msgid "When using :ref:`UseCashRegisters <usecashregisters-label>`, selecting a cash register will always be required when the CASH payment type is chosen. This system preference determines if selecting a cash register is required when choosing another payment type."
msgstr ""

#: ../../source/accountspreferences.rst:104
msgid "UseCashRegisters"
msgstr "UseCashRegisters"

# Accounting > Features
#: ../../source/accountspreferences.rst:106
#, fuzzy
msgid "Asks: \\_\\_\\_ cash registers with the accounting system to track payments."
msgstr "السجلات النقدية مع نظام المحاسبة لتتبع المدفوعات."

#: ../../source/accountspreferences.rst:108
msgid "Default: Don't use"
msgstr "الإفتراضي: لا تستخدم"

#: ../../source/accountspreferences.rst:112
msgid "Don't use"
msgstr "لا تستخدم"

#: ../../source/accountspreferences.rst:114
msgid "Use"
msgstr "استخدم"

#: ../../source/accountspreferences.rst:118
msgid "This preference enables the :ref:`cash registers <cashregisters-label>` feature in the administration module."
msgstr ""

#: ../../source/accountspreferences.rst:124
msgid "Policy"
msgstr "السياسة"

# Accounting > Policy
#: ../../source/accountspreferences.rst:129
#, fuzzy
msgid "AccountAutoReconcile"
msgstr "القيام بـ"

# Accounting > Policy
#: ../../source/accountspreferences.rst:131
#, fuzzy
msgid "Asks: \\_\\_\\_ reconcile patron balances automatically on each transaction adding debits or credits."
msgstr "تسوية حسابات المستفيدين تلقائيًا عند كل معاملة لإضافة أرصدة مدينة أو ائتمانات"

#: ../../source/accountspreferences.rst:133
msgid "Default: Do not"
msgstr "الإفتراضي: لا تنفذ"

#: ../../source/accountspreferences.rst:137
#: ../../source/accountspreferences.rst:158
#: ../../source/accountspreferences.rst:206
msgid "Do"
msgstr "نفذ"

#: ../../source/accountspreferences.rst:139
msgid "Do not"
msgstr "لا تنفذ"

#: ../../source/accountspreferences.rst:143
msgid "This preference controls whether or not credits are automatically used to to reduce the owed amounts in a patron's account."
msgstr ""

# Accounting > Policy
#: ../../source/accountspreferences.rst:149
#, fuzzy
msgid "FinePaymentAutoPopup"
msgstr "القيام بـ"

# Accounting > Policy
#: ../../source/accountspreferences.rst:151
#, fuzzy
msgid "Asks: \\_\\_\\_ automatically display a print dialog for a payment receipt when making a payment."
msgstr "قم بعرض محادثة الطباعة تلقائيًا لإيصال السداد عند عملية الدفع."

#: ../../source/accountspreferences.rst:154
#: ../../source/accountspreferences.rst:200
msgid "Default: Don't"
msgstr "افتراضي : لا تنفذ"

#: ../../source/accountspreferences.rst:160
#: ../../source/accountspreferences.rst:204
msgid "Don't"
msgstr "لا تنفذ"

#: ../../source/accountspreferences.rst:164
msgid "If activated, when :ref:`making a payment <pay-and-writeoff-fines-label>` in a patron's account, a printing popup will be displayed automatically instead of having to click on the 'print' button."
msgstr ""

#: ../../source/accountspreferences.rst:171
msgid "RequirePaymentType"
msgstr ""

#: ../../source/accountspreferences.rst:173
msgid "Asks: \\_\\_\\_ staff to select a payment type when a payment is made."
msgstr ""

#: ../../source/accountspreferences.rst:175
#, fuzzy
msgid "Default: Do not require"
msgstr "الإفتراضي: لا تنفذ"

#: ../../source/accountspreferences.rst:179
#, fuzzy
msgid "Do not require"
msgstr "لا تنفذ"

#: ../../source/accountspreferences.rst:181
msgid "Require"
msgstr ""

#: ../../source/accountspreferences.rst:185
msgid "This system preference is only used if the :ref:`UseCashRegisters <usecashregisters-label>` system preference disabled."
msgstr ""

#: ../../source/accountspreferences.rst:189
msgid "This system preference is used to make the 'Payment type' field mandatory when :ref:`paying charges <pay-and-writeoff-fines-label>` in a patron's account."
msgstr ""

# Accounting > Policy
#: ../../source/accountspreferences.rst:196
#, fuzzy
msgid "RoundFinesAtPayment"
msgstr "القيام بـ"

# OPAC > Features
#: ../../source/accountspreferences.rst:198
#, fuzzy
msgid "Asks: \\_\\_\\_ round fines to the nearest cent when collecting payments."
msgstr "الصيغة: \\_\\_\\_ للمستخدمين بإضافة ملاحظة عند وضع حجز."

# Accounting > Policy
#: ../../source/accountspreferences.rst:210
#, fuzzy
msgid "Enabling this preference allows paying fines of partial cents which may not be visible in the interface."
msgstr "قم بتقريب الغرامات إلى أقرب سنت عند تحصيل المدفوعات. يتيح تفعيل هذا التفضيل دفع غرامات جزئية والتي قد لا تكون مرئية في الواجهة."
