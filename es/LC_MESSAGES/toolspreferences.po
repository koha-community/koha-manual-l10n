# Spanish translations for Koha Manual package.
# Copyright (C) 2020, Koha Community
# This file is distributed under the same license as the Koha Manual package.
# Automatically generated, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: Koha Manual 22.11\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-02-03 01:01+0000\n"
"PO-Revision-Date: 2021-03-01 20:19-0300\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Pootle-Path: /es/manual22.11/toolspreferences.po\n"
"X-Pootle-Revision: 1\n"

#: ../../source/toolspreferences.rst:6
msgid "Tools"
msgstr ""

#: ../../source/toolspreferences.rst:8
msgid "*Get there:* More > Administration > System preferences > Tools"
msgstr ""

#: ../../source/toolspreferences.rst:13
msgid "Barcodes"
msgstr ""

#: ../../source/toolspreferences.rst:18
msgid "BarcodeSeparators"
msgstr ""

#: ../../source/toolspreferences.rst:20
msgid "Asks: Split barcodes on the following separator chars \\_\\_\\_ in batch item modification and inventory."
msgstr ""

#: ../../source/toolspreferences.rst:23
msgid "Default: \\\\s\\\\r\\\\n"
msgstr ""

#: ../../source/toolspreferences.rst:25 ../../source/toolspreferences.rst:70
#: ../../source/toolspreferences.rst:90 ../../source/toolspreferences.rst:110
#: ../../source/toolspreferences.rst:141 ../../source/toolspreferences.rst:179
#: ../../source/toolspreferences.rst:202 ../../source/toolspreferences.rst:227
#: ../../source/toolspreferences.rst:251
msgid "Description:"
msgstr ""

#: ../../source/toolspreferences.rst:27
msgid "When importing barcode files in the :ref:`inventory tool <inventory-tool-label>` or the :ref:`batch item modification tool <batch-item-modification-label>` you can decide which character separates each barcode."
msgstr ""

#: ../../source/toolspreferences.rst:33
msgid "You must use the regular expression codes for the characters."
msgstr ""

#: ../../source/toolspreferences.rst:35
msgid "\\\\s is used for a whitespace"
msgstr ""

#: ../../source/toolspreferences.rst:37
msgid "\\\\r is used for a carriage return"
msgstr ""

#: ../../source/toolspreferences.rst:39
msgid "\\\\n is used for a new line"
msgstr ""

#: ../../source/toolspreferences.rst:41
msgid "\\\\t is used for a tab"
msgstr ""

#: ../../source/toolspreferences.rst:43
msgid "Make sure you escape the other characters you put in there by preceding them with a backslash"
msgstr ""

#: ../../source/toolspreferences.rst:46
msgid "\\\\. instead of a dot"
msgstr ""

#: ../../source/toolspreferences.rst:48
msgid "\\\\\\\\ instead of a backslash"
msgstr ""

#: ../../source/toolspreferences.rst:50
msgid "\\\\- instead of a hyphen"
msgstr ""

#: ../../source/toolspreferences.rst:55
msgid "Batch item"
msgstr ""

#: ../../source/toolspreferences.rst:57
msgid "These preferences are in reference to the :ref:`Batch item modification <batch-item-modification-label>` and :ref:`Batch item deletion <batch-item-deletion-label>` tools."
msgstr ""

#: ../../source/toolspreferences.rst:64
msgid "MaxItemsToDisplayForBatchDel"
msgstr ""

#: ../../source/toolspreferences.rst:66
msgid "Asks: Display up to \\_\\_\\_ items in a single item deletion batch."
msgstr ""

#: ../../source/toolspreferences.rst:68 ../../source/toolspreferences.rst:88
#: ../../source/toolspreferences.rst:108
msgid "Default: 1000"
msgstr ""

#: ../../source/toolspreferences.rst:72
msgid "In the :ref:`batch item delete tool <batch-item-deletion-label>` this will prevent the display of more than the items you entered in this preference, but you will be able to delete more than the number you enter here."
msgstr ""

#: ../../source/toolspreferences.rst:79 ../../source/toolspreferences.rst:99
msgid "Displaying a large number of items may slow down the display."
msgstr ""

#: ../../source/toolspreferences.rst:84
msgid "MaxItemsToDisplayForBatchMod"
msgstr ""

#: ../../source/toolspreferences.rst:86
msgid "Asks: Display up to \\_\\_\\_ items in a single item modification batch."
msgstr ""

#: ../../source/toolspreferences.rst:92
msgid "In the :ref:`batch item modification tool <batch-item-modification-label>` this will prevent the display of more than the items you entered in this preference, but you will be able to modify more than the number you enter here (see :ref:`MaxItemsToProcessForBatchMod<maxitemsforbatchmod-label>`)."
msgstr ""

#: ../../source/toolspreferences.rst:104
msgid "MaxItemsToProcessForBatchMod"
msgstr ""

#: ../../source/toolspreferences.rst:106
msgid "Asks: Process up to \\_\\_\\_ items in a single modification batch."
msgstr ""

#: ../../source/toolspreferences.rst:112
msgid "In the :ref:`batch item modification tool <batch-item-modification-label>` this preference will prevent the editing of more than the number entered here."
msgstr ""

#: ../../source/toolspreferences.rst:118
msgid "Processing a large number of items may slow down the process and it may even time out."
msgstr ""

#: ../../source/toolspreferences.rst:124
msgid "News"
msgstr ""

#: ../../source/toolspreferences.rst:129
msgid "AdditionalContentsEditor"
msgstr ""

#: ../../source/toolspreferences.rst:131
msgid "Asks: By default edit additional contents and news items with"
msgstr ""

#: ../../source/toolspreferences.rst:133
msgid "Default: a WYSIWYG editor (TinyMCE)"
msgstr ""

#: ../../source/toolspreferences.rst:135 ../../source/toolspreferences.rst:169
msgid "Values:"
msgstr ""

#: ../../source/toolspreferences.rst:137
msgid "a text editor (CodeMirror)"
msgstr ""

#: ../../source/toolspreferences.rst:139
msgid "a WYSIWYG editor (TinyMCE)"
msgstr ""

#: ../../source/toolspreferences.rst:143
msgid "This system preference allows you to choose which editor to use in the :ref:`news tool <news-label>` and the :ref:`HTML customizations tool <html-customizations-label>`."
msgstr ""

#: ../../source/toolspreferences.rst:147
msgid "A text editor will let you write HTML directly. Use this if you want more control over how your content will be displayed."
msgstr ""

#: ../../source/toolspreferences.rst:152
msgid "You can use CSS id's and classes in your HTML and format them with the :ref:`OPACUserCSS <opacusercss-label>` or :ref:`IntranetUserCSS <intranetusercss-label>` system preferences."
msgstr ""

#: ../../source/toolspreferences.rst:156
msgid "A WYSIWYG (what you see is what you get) editor is a user-friendly editor with functions similar to word processing software. Use this if you are unfamiliar with HTML."
msgstr ""

#: ../../source/toolspreferences.rst:163
msgid "NewsAuthorDisplay"
msgstr ""

#: ../../source/toolspreferences.rst:165
msgid "Asks: Show the author for news items: \\_\\_\\_"
msgstr ""

#: ../../source/toolspreferences.rst:167
msgid "Default: not at all"
msgstr ""

#: ../../source/toolspreferences.rst:171
msgid "Both OPAC and staff interface"
msgstr ""

#: ../../source/toolspreferences.rst:173
msgid "Not at all"
msgstr ""

#: ../../source/toolspreferences.rst:175
msgid "OPAC only"
msgstr ""

#: ../../source/toolspreferences.rst:177
msgid "Staff interface only"
msgstr ""

#: ../../source/toolspreferences.rst:181
msgid "This system preference lets you choose whether or not the name of the author of the news item is displayed and where."
msgstr ""

#: ../../source/toolspreferences.rst:187
msgid "Patron cards"
msgstr ""

#: ../../source/toolspreferences.rst:189
msgid "These preferences are in reference to the :ref:`Patron card creator <patron-card-creator-label>` tool."
msgstr ""

#: ../../source/toolspreferences.rst:195
msgid "ImageLimit"
msgstr ""

#: ../../source/toolspreferences.rst:197
msgid "Asks: Limit the number of creator images stored in the database to \\_\\_\\_ images."
msgstr ""

#: ../../source/toolspreferences.rst:200
msgid "Default: 5"
msgstr ""

#: ../../source/toolspreferences.rst:204
msgid "This system preference determines the number of images that can be used in the :ref:`patron card creator tool <patron-card-creator-label>`."
msgstr ""

#: ../../source/toolspreferences.rst:209
msgid "A large number of images may take a lot of storage space."
msgstr ""

#: ../../source/toolspreferences.rst:214
msgid "Reports"
msgstr ""

#: ../../source/toolspreferences.rst:216
msgid "These preferences are in reference to the :ref:`reports module <reports-label>`."
msgstr ""

#: ../../source/toolspreferences.rst:221
msgid "NumSavedReports"
msgstr ""

#: ../../source/toolspreferences.rst:223
msgid "Asks: By default, show \\_\\_\\_ reports on the saved reports page."
msgstr ""

#: ../../source/toolspreferences.rst:225
msgid "Default: 20"
msgstr ""

#: ../../source/toolspreferences.rst:229
msgid "This system preference lets you determine the number of reports that should be displayed per page in the saved reports page."
msgstr ""

#: ../../source/toolspreferences.rst:234
msgid "Displaying a large number of reports may slow down the display."
msgstr ""

#: ../../source/toolspreferences.rst:239
msgid "Upload"
msgstr ""

#: ../../source/toolspreferences.rst:244
msgid "UploadPurgeTemporaryFilesDays"
msgstr ""

#: ../../source/toolspreferences.rst:246
msgid "Asks: Automatically delete temporary uploads older than \\_\\_\\_ days in cleanup\\_database cron job."
msgstr ""

#: ../../source/toolspreferences.rst:249
msgid "Default: blank"
msgstr ""

#: ../../source/toolspreferences.rst:253
msgid "This system preference lets you determine how long temporary uploaded files (files uploaded through the :ref:`upload tool <upload-label>` without any category) are kept"
msgstr ""

#: ../../source/toolspreferences.rst:259
msgid "If you leave this field empty, the cron job will not delete any files."
msgstr ""

#: ../../source/toolspreferences.rst:261
msgid "On the other hand, a value of 0 means 'delete all temporary files'."
msgstr ""

#: ../../source/toolspreferences.rst:263
msgid "This value can be overridden with the --temp-uploads-days option of the cleanup\\_database script."
msgstr ""
