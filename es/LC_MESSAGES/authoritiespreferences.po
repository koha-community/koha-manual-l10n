# Spanish translations for Koha Manual package.
# Copyright (C) 2020, Koha Community
# This file is distributed under the same license as the Koha Manual package.
# Automatically generated, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: Koha Manual 22.11\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2025-01-01 01:00+0000\n"
"PO-Revision-Date: 2021-12-16 19:24+0000\n"
"Last-Translator: Tomás Cohen Arazi <tomascohen@gmail.com>\n"
"Language-Team: none\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Pootle 2.8\n"
"X-Pootle-Path: /es/manual22.11/authoritiespreferences.po\n"
"X-Pootle-Revision: 1\n"
"X-POOTLE-MTIME: 1639682654.223346\n"

#: ../../source/authoritiespreferences.rst:6
msgid "Authorities"
msgstr ""

#: ../../source/authoritiespreferences.rst:8
msgid "*Get there:* More > Administration > System preferences > Authorities"
msgstr ""

#: ../../source/authoritiespreferences.rst:14
msgid "General"
msgstr ""

#: ../../source/authoritiespreferences.rst:19
msgid "AdditionalFieldsInZ3950ResultAuthSearch"
msgstr ""

#: ../../source/authoritiespreferences.rst:21
#: ../../source/authoritiespreferences.rst:206
msgid "Version"
msgstr ""

#: ../../source/authoritiespreferences.rst:23
#: ../../source/authoritiespreferences.rst:208
msgid "This system preference was introduced in version 23.11 of Koha."
msgstr ""

#: ../../source/authoritiespreferences.rst:25
msgid "Asks: Display the MARC field/subfields \\_\\_\\_ in the 'Additional fields' column of Z39.50 search results (use comma as delimiter e.g.: \"001, 035$a\")."
msgstr ""

#: ../../source/authoritiespreferences.rst:27
#: ../../source/authoritiespreferences.rst:46
#: ../../source/authoritiespreferences.rst:113
#: ../../source/authoritiespreferences.rst:136
#: ../../source/authoritiespreferences.rst:181
#: ../../source/authoritiespreferences.rst:237
#: ../../source/authoritiespreferences.rst:258
#: ../../source/authoritiespreferences.rst:284
#: ../../source/authoritiespreferences.rst:316
#: ../../source/authoritiespreferences.rst:336
#: ../../source/authoritiespreferences.rst:367
#: ../../source/authoritiespreferences.rst:395
#: ../../source/authoritiespreferences.rst:434
#: ../../source/authoritiespreferences.rst:468
#: ../../source/authoritiespreferences.rst:499
#: ../../source/authoritiespreferences.rst:549
#: ../../source/authoritiespreferences.rst:587
#: ../../source/authoritiespreferences.rst:604
#: ../../source/authoritiespreferences.rst:641
msgid "Description:"
msgstr ""

#: ../../source/authoritiespreferences.rst:29
msgid "This system preference lets you display additional information from the MARC record in the authority Z39.50/SRU search result lists."
msgstr ""

#: ../../source/authoritiespreferences.rst:34
msgid "AuthDisplayHierarchy"
msgstr ""

#: ../../source/authoritiespreferences.rst:36
msgid "Asks: \\_\\_\\_ broader term/narrower term hierarchies when viewing authorities."
msgstr ""

#: ../../source/authoritiespreferences.rst:38
#, fuzzy
msgid "Default: Don't show"
msgstr "Predeterminado: No mostrar"

#: ../../source/authoritiespreferences.rst:40
#: ../../source/authoritiespreferences.rst:160
#: ../../source/authoritiespreferences.rst:175
#: ../../source/authoritiespreferences.rst:213
#: ../../source/authoritiespreferences.rst:330
#: ../../source/authoritiespreferences.rst:383
#: ../../source/authoritiespreferences.rst:428
#: ../../source/authoritiespreferences.rst:462
#: ../../source/authoritiespreferences.rst:493
#: ../../source/authoritiespreferences.rst:543
#: ../../source/authoritiespreferences.rst:565
#: ../../source/authoritiespreferences.rst:635
msgid "Values:"
msgstr ""

#: ../../source/authoritiespreferences.rst:42
msgid "Don't show"
msgstr ""

#: ../../source/authoritiespreferences.rst:44
msgid "Show"
msgstr ""

#: ../../source/authoritiespreferences.rst:48
msgid "When set to 'Show' broader and narrower 'See also' references will be displayed in a tree at the top of the record in both the staff interface and the OPAC."
msgstr ""

#: ../../source/authoritiespreferences.rst:52
msgid "|AuthDisplayHierarchy|"
msgstr ""

#: ../../source/authoritiespreferences.rst:54
msgid "For MARC21, in order for this display to appear, the records must have 5XX with the relationship code in $w and the authority number (authid) in $9."
msgstr ""

#: ../../source/authoritiespreferences.rst:59
msgid "The relationship codes in $w can be found on the Library of Congress website under `Tracings and references-General information <https://www.loc.gov/marc/authority/adtracing.html>`_"
msgstr ""

#: ../../source/authoritiespreferences.rst:62
msgid "g - Broader term"
msgstr ""

#: ../../source/authoritiespreferences.rst:64
msgid "h - Narrower term"
msgstr ""

#: ../../source/authoritiespreferences.rst:66
msgid "For example, the records in the above screenshot have the following information::"
msgstr ""

#: ../../source/authoritiespreferences.rst:95
msgid "The relationships must be bidirectional, meaning the broader term must reference the narrower term and the narrower term must reference the broader term."
msgstr ""

#: ../../source/authoritiespreferences.rst:102
msgid "AuthorityControlledIndicators"
msgstr ""

#: ../../source/authoritiespreferences.rst:104
msgid "Asks: Use the following text to edit how authority records control indicators of attached biblio fields (and possibly subfield $2). Lines starting with a comment symbol (#) are skipped. Each line should be of the form: (marc21|unimarc), tag, ind1:(auth1|auth2|some_value), ind2:(auth1|auth2|thesaurus|some_value). Here auth1 and auth2 refer to the indicators of the authority record, tag is a biblio field number or an asterisk (*), and some_value is a fixed value (one character). The MARC21 option thesaurus refers to indicators controlled by authority field 008/11 and 040$f."
msgstr ""

#: ../../source/authoritiespreferences.rst:115
msgid "Used when merging authorities."
msgstr ""

#: ../../source/authoritiespreferences.rst:117
msgid "Controls how the indicators of linked authority records affect the corresponding biblio indicators. Currently, the default value is fine-tuned for MARC21 and copies the authority indicators for UNIMARC."
msgstr ""

#: ../../source/authoritiespreferences.rst:121
msgid "For example, a MARC21 field 100 in a biblio record should pick its first indicator from the linked authority record. The second indicator is not controlled by the authority. This report supports such MARC conventions."
msgstr ""

#: ../../source/authoritiespreferences.rst:128
msgid "AuthorityMergeLimit"
msgstr ""

#: ../../source/authoritiespreferences.rst:130
msgid "Asks: When modifying an authority record, do not update attached bibliographic records if the number exceeds \\_\\_\\_ records. (Above this limit, the :ref:`merge_authority cron job<cron-update-authorities-label>` will update them.)"
msgstr ""

#: ../../source/authoritiespreferences.rst:134
msgid "Default: 50"
msgstr "Predeterminado: 50"

#: ../../source/authoritiespreferences.rst:138
msgid "This system preference determines the maximum number of bibliographic records that can be updated when an authority record changes."
msgstr ""

#: ../../source/authoritiespreferences.rst:141
msgid "This helps prevent overusing resources if an authority record is linked to many bibliographic records."
msgstr ""

#: ../../source/authoritiespreferences.rst:144
msgid "Make sure to enable the :ref:`merge_authority cron job<cron-update-authorities-label>` to catch the updates that wouldn't otherwise be transferred to bibliographic records."
msgstr ""

#: ../../source/authoritiespreferences.rst:151
msgid "AuthorityMergeMode"
msgstr ""

#: ../../source/authoritiespreferences.rst:153
msgid "Default: loose"
msgstr ""

#: ../../source/authoritiespreferences.rst:155
msgid "Asks: When updating biblio records from an attached authority record (\"merging\"), handle subfields of relevant biblio record fields in \\_\\_\\_ mode. In strict mode subfields that are not found in the authority record, are deleted. Loose mode will keep them. Loose mode is the historical behavior and still the default."
msgstr ""

#: ../../source/authoritiespreferences.rst:162
msgid "loose"
msgstr ""

#: ../../source/authoritiespreferences.rst:164
msgid "strict"
msgstr ""

#: ../../source/authoritiespreferences.rst:169
msgid "AutoCreateAuthorities"
msgstr ""

#: ../../source/authoritiespreferences.rst:171
msgid "Asks: When editing records, \\_\\_\\_ authority records that are missing."
msgstr ""

#: ../../source/authoritiespreferences.rst:173
#, fuzzy
msgid "Default: don't generate"
msgstr "Predeterminado: No mostrar"

#: ../../source/authoritiespreferences.rst:177
msgid "don't generate"
msgstr ""

#: ../../source/authoritiespreferences.rst:179
msgid "generate"
msgstr ""

#: ../../source/authoritiespreferences.rst:183
msgid "This system preference allows you to decide if Koha automatically creates new authority records from the bibliographic records."
msgstr ""

#: ../../source/authoritiespreferences.rst:188
msgid ":ref:`RequireChoosingExistingAuthority <requirechoosingexistingauthority-label>` must be set to \"don't require\", otherwise this system preference has no effect."
msgstr ""

#: ../../source/authoritiespreferences.rst:192
msgid "If this is set to \"don't generate\", and catalogers enter uncontrolled terms in controlled fields, Koha will not generate matching authority records."
msgstr ""

#: ../../source/authoritiespreferences.rst:195
msgid "If set to \"generate\" (and :ref:`RequireChoosingExistingAuthority <requirechoosingexistingauthority-label>` is set to \"don't require\"), Koha will automatically create authority records for headings that don't have any authority link when saving a bibliographic record."
msgstr ""

#: ../../source/authoritiespreferences.rst:204
msgid "DefaultAuthorityTab"
msgstr ""

#: ../../source/authoritiespreferences.rst:210
msgid "Asks: When viewing authority details default to the \\_\\_\\_  tab. If the selected tab does not exist, the view will default to the first tab."
msgstr ""

#: ../../source/authoritiespreferences.rst:215
msgid "0XX"
msgstr ""

#: ../../source/authoritiespreferences.rst:217
msgid "1XX"
msgstr ""

#: ../../source/authoritiespreferences.rst:219
msgid "2XX"
msgstr ""

#: ../../source/authoritiespreferences.rst:221
msgid "3XX"
msgstr ""

#: ../../source/authoritiespreferences.rst:223
msgid "4XX"
msgstr ""

#: ../../source/authoritiespreferences.rst:225
msgid "5XX"
msgstr ""

#: ../../source/authoritiespreferences.rst:227
msgid "6XX"
msgstr ""

#: ../../source/authoritiespreferences.rst:229
msgid "7XX"
msgstr ""

#: ../../source/authoritiespreferences.rst:231
msgid "8XX"
msgstr ""

#: ../../source/authoritiespreferences.rst:233
msgid "9XX"
msgstr ""

#: ../../source/authoritiespreferences.rst:235
#, fuzzy
msgid "Default: 0XX"
msgstr "Predeterminado: 50"

#: ../../source/authoritiespreferences.rst:239
msgid "This system preference allows the library to choose which tab is presented first when viewing an authority record."
msgstr ""

#: ../../source/authoritiespreferences.rst:242
msgid "By default, the \"0\" tab, containing all 0XX MARC fields, is selected when first viewing an authority record."
msgstr ""

#: ../../source/authoritiespreferences.rst:245
msgid "If the selected tab doesn't exist for that particular authority record, the first tab (usually tab \"0\"), will be selected by default."
msgstr ""

#: ../../source/authoritiespreferences.rst:251
msgid "GenerateAuthorityField667"
msgstr ""

#: ../../source/authoritiespreferences.rst:253
msgid "Asks: Use the following text as default value for the 667$a field of MARC21 authority records: \\_\\_\\_"
msgstr ""

#: ../../source/authoritiespreferences.rst:256
msgid "Default: Machine generated authority record"
msgstr ""

#: ../../source/authoritiespreferences.rst:260
#: ../../source/authoritiespreferences.rst:286
msgid "This system preference is used when Koha automatically creates authority records when :ref:`RequireChoosingExistingAuthority <requirechoosingexistingauthority-label>` is set to \"don't require\" and :ref:`AutoCreateAuthorities <AutoCreateAuthorities-label>` is set to \"generate\"."
msgstr ""

#: ../../source/authoritiespreferences.rst:267
msgid "Enter the text you want to use in the 667$a field."
msgstr ""

#: ../../source/authoritiespreferences.rst:271
#: ../../source/authoritiespreferences.rst:297
msgid "This system preference is only used when :ref:`marcflavour <marcflavour-label>` is set to MARC21."
msgstr ""

#: ../../source/authoritiespreferences.rst:277
msgid "GenerateAuthorityField670"
msgstr ""

#: ../../source/authoritiespreferences.rst:279
msgid "Asks: Use the following text as default value for the 670$a field of MARC21 authority records: \\_\\_\\_"
msgstr ""

#: ../../source/authoritiespreferences.rst:282
#, fuzzy
msgid "Default: Work cat."
msgstr "Predeterminado: 50"

#: ../../source/authoritiespreferences.rst:293
msgid "Enter the text you want to use in the 670$a field."
msgstr ""

#: ../../source/authoritiespreferences.rst:303
msgid "MARCAuthorityControlField008"
msgstr ""

#: ../../source/authoritiespreferences.rst:305
msgid "Default: \\|\\| aca\\|\\|aabn \\| a\\|a d"
msgstr ""

#: ../../source/authoritiespreferences.rst:307
msgid "Asks: Use the following text for the contents of MARC21 authority control field 008 position 06-39 (fixed length data elements)."
msgstr ""

#: ../../source/authoritiespreferences.rst:310
#: ../../source/authoritiespreferences.rst:361
#: ../../source/authoritiespreferences.rst:599
msgid "**Important**"
msgstr ""

#: ../../source/authoritiespreferences.rst:312
msgid "Do not include the date (position 00-05) in this preference, Koha will calculate automatically and put that in before the values in this preference."
msgstr ""

#: ../../source/authoritiespreferences.rst:318
msgid "This preference controls the default value in the 008 field on Authority records. It does not effect bibliographic records."
msgstr ""

#: ../../source/authoritiespreferences.rst:324
msgid "RequireChoosingExistingAuthority"
msgstr ""

#: ../../source/authoritiespreferences.rst:326
msgid "Asks: When editing records, \\_\\_\\_ catalogers to reference existing authorities."
msgstr ""

#: ../../source/authoritiespreferences.rst:328
#, fuzzy
msgid "Default: don't require"
msgstr "Predeterminado: No mostrar"

#: ../../source/authoritiespreferences.rst:332
msgid "don't require"
msgstr ""

#: ../../source/authoritiespreferences.rst:334
msgid "require"
msgstr ""

#: ../../source/authoritiespreferences.rst:338
msgid "This system preference determines whether or not catalogers can type into authority controlled fields."
msgstr ""

#: ../../source/authoritiespreferences.rst:341
msgid "If set to \"don't require\", catalogers can manually enter uncontrolled terms into controlled fields"
msgstr ""

#: ../../source/authoritiespreferences.rst:344
msgid "If set to \"require\", authority controlled fields will be locked, forcing catalogers to search for an authority."
msgstr ""

#: ../../source/authoritiespreferences.rst:347
msgid "If set to \"require\", it will block :ref:`AutoCreateAuthorities <AutoCreateAuthorities-label>`. Authorities will have to be catalogued manually (or imported)."
msgstr ""

#: ../../source/authoritiespreferences.rst:354
msgid "UNIMARCAuthorityField100"
msgstr ""

#: ../../source/authoritiespreferences.rst:356
msgid "Default: afrey50 ba0"
msgstr ""

#: ../../source/authoritiespreferences.rst:358
msgid "Asks: Use the following text for the contents of UNIMARC authority field 100 position 08-35 (fixed length data elements)."
msgstr ""

#: ../../source/authoritiespreferences.rst:363
msgid "Do not include the date (position 00-07) in this preference, Koha will calculate automatically and put that in before the values in this preference."
msgstr ""

#: ../../source/authoritiespreferences.rst:369
msgid "This preference controls the default value in the 100 field on Authority records cataloged in UNIMARC. It does not effect bibliographic records."
msgstr ""

#: ../../source/authoritiespreferences.rst:376
msgid "UseAuthoritiesForTracings"
msgstr ""

#: ../../source/authoritiespreferences.rst:378
msgid "Default: Don't use"
msgstr ""

#: ../../source/authoritiespreferences.rst:380
msgid "Asks: \\_\\_\\_ authority record numbers instead of text strings for searches from subject tracings."
msgstr ""

#: ../../source/authoritiespreferences.rst:385
msgid "Don't use"
msgstr ""

#: ../../source/authoritiespreferences.rst:387
msgid "Search links look for subject/author keywords (example: opac-search.pl?q=su:Business%20networks)"
msgstr ""

#: ../../source/authoritiespreferences.rst:390
msgid "Use"
msgstr ""

#: ../../source/authoritiespreferences.rst:392
msgid "Search links look for an authority record (example: opac-search.pl?q=an:354)"
msgstr ""

#: ../../source/authoritiespreferences.rst:397
msgid "For libraries that have authority files, they may want to make it so that when a link to an authorized subject or author is clicked on the OPAC or staff client it takes the searcher only to a list of results with that authority record. Most libraries do not have complete authority files and so setting this preference to 'Don't use' will allow searchers to click on links to authors and subject headings and perform a keyword search against those fields, finding all possible relevant results instead."
msgstr ""

#: ../../source/authoritiespreferences.rst:409
msgid "Linker"
msgstr ""

#: ../../source/authoritiespreferences.rst:411
msgid "These preferences will control how Koha links bibliographic records to authority records. All bibliographic records added to Koha after these preferences are set will link automatically to authority records, for records added before these preferences are set there is a script (misc/link\\_bibs\\_to\\_authorities.pl) that your system administrator can run to link records together."
msgstr ""

#: ../../source/authoritiespreferences.rst:421
msgid "AutoLinkBiblios"
msgstr ""

#: ../../source/authoritiespreferences.rst:423
msgid "Asks: \\_\\_\\_ attempt to automatically link headings when saving records in the cataloging module."
msgstr ""

#: ../../source/authoritiespreferences.rst:426
#: ../../source/authoritiespreferences.rst:460
#: ../../source/authoritiespreferences.rst:491
#, fuzzy
msgid "Default: Don't"
msgstr "Predeterminado: No mostrar"

#: ../../source/authoritiespreferences.rst:430
#: ../../source/authoritiespreferences.rst:464
#: ../../source/authoritiespreferences.rst:497
#: ../../source/authoritiespreferences.rst:545
#: ../../source/authoritiespreferences.rst:637
msgid "Do"
msgstr ""

#: ../../source/authoritiespreferences.rst:432
#: ../../source/authoritiespreferences.rst:466
#: ../../source/authoritiespreferences.rst:495
msgid "Don't"
msgstr ""

#: ../../source/authoritiespreferences.rst:436
msgid "This system preference determines whether or not Koha will attempt to link terms in authority controlled field to existing authority records upon saving a new bibliographic record."
msgstr ""

#: ../../source/authoritiespreferences.rst:440
msgid "This system preference is used when creating new bibliographic records. :ref:`CatalogModuleRelink <catalogmodulerelink-label>` and :ref:`LinkerRelink <linkerrelink-label>` are used when editing bibliographic records."
msgstr ""

#: ../../source/authoritiespreferences.rst:445
msgid "If set to \"Do\", Koha will search in the thesaurus to find a matching authority record. If it finds one, it will link it. If it finds more than one, :ref:`LinkerModule <linkermodule-label>` will determine which one is linked."
msgstr ""

#: ../../source/authoritiespreferences.rst:449
msgid "If set to \"Don't\", no linking attempt will be made when saving a new bibliographic record."
msgstr ""

#: ../../source/authoritiespreferences.rst:455
msgid "CatalogModuleRelink"
msgstr ""

#: ../../source/authoritiespreferences.rst:457
msgid "Asks: \\_\\_\\_ automatically relink headings that have previously been linked when saving records in the cataloging module."
msgstr ""

#: ../../source/authoritiespreferences.rst:470
msgid "This system preference determines whether or not authority links are checked and adjusted when saving a bibliographic record."
msgstr ""

#: ../../source/authoritiespreferences.rst:473
msgid "When set to \"Do\", Koha will automatically relink headings when a record is saved in the cataloging module when :ref:`LinkerRelink <linkerrelink-label>` is turned on, even if the headings were manually linked to a different authority by the cataloger."
msgstr ""

#: ../../source/authoritiespreferences.rst:480
msgid "This system preference requires that :ref:`AutoLinkBiblios <autolinkbiblios-label>` be set to \"Do\"."
msgstr ""

#: ../../source/authoritiespreferences.rst:486
msgid "LinkerConsiderThesaurus"
msgstr ""

#: ../../source/authoritiespreferences.rst:488
msgid "Asks: \\_\\_\\_ compare the source for 6XX headings to the thesaurus source for authority records when linking."
msgstr ""

#: ../../source/authoritiespreferences.rst:501
msgid "This system preference is used to enable multiple thesauri support for authorities."
msgstr ""

#: ../../source/authoritiespreferences.rst:504
msgid "The thesaurus used for the term is added to the authority record using 040$f."
msgstr ""

#: ../../source/authoritiespreferences.rst:507
msgid "In the bibliographic record, set the second indicator to 7 for 650 and add the source to 650$2. For local terms, use 4 (Source not specified) as the indicator."
msgstr ""

#: ../../source/authoritiespreferences.rst:511
msgid "Example for a bibliographic record::"
msgstr ""

#: ../../source/authoritiespreferences.rst:517
msgid "The first example above is the LCSH term. The other two terms are from sao (controlled Swedish subject heading system) and barn (Swedish children subject heading system). These three are using the same TOPIC_TERM Feminism, but they belong to different thesauri."
msgstr ""

#: ../../source/authoritiespreferences.rst:524
msgid "In existing systems, enabling this preference may require a reindex, and may generate new authority records if :ref:`AutoCreateAuthorities <autocreateauthorities-label>` is enabled."
msgstr ""

#: ../../source/authoritiespreferences.rst:530
msgid "Support for multiple authority thesauri currently only works with ElasticSearch."
msgstr ""

#: ../../source/authoritiespreferences.rst:536
msgid "LinkerKeepStale"
msgstr ""

#: ../../source/authoritiespreferences.rst:538
msgid "Default: Do not"
msgstr ""

#: ../../source/authoritiespreferences.rst:540
msgid "Asks: \\_\\_\\_ keep existing links to authority records for headings where the linker is unable to find a match."
msgstr ""

#: ../../source/authoritiespreferences.rst:547
#: ../../source/authoritiespreferences.rst:639
msgid "Do not"
msgstr ""

#: ../../source/authoritiespreferences.rst:551
msgid "When set to 'Do', the linker will never remove a link to an authority record, though, depending on the value of :ref:`LinkerRelink <LinkerRelink-label>`, it may change the link."
msgstr ""

#: ../../source/authoritiespreferences.rst:558
msgid "LinkerModule"
msgstr ""

#: ../../source/authoritiespreferences.rst:560
msgid "Default: Default"
msgstr ""

#: ../../source/authoritiespreferences.rst:562
msgid "Asks: Use the \\_\\_\\_ linker module for matching headings to authority records."
msgstr ""

#: ../../source/authoritiespreferences.rst:567
msgid "Default"
msgstr ""

#: ../../source/authoritiespreferences.rst:569
msgid "Retains Koha's previous behavior of only creating links when there is an exact match to one and only one authority record; if the :ref:`LinkerOptions <LinkerOptions-label>` preference is set to 'broader\\_headings', it will try to link headings to authority records for broader headings by removing subfields from the end of the heading"
msgstr ""

#: ../../source/authoritiespreferences.rst:576
msgid "First match"
msgstr ""

#: ../../source/authoritiespreferences.rst:578
msgid "Creates a link to the first authority record that matches a given heading, even if there is more than one authority record that matches"
msgstr ""

#: ../../source/authoritiespreferences.rst:582
msgid "Last match"
msgstr ""

#: ../../source/authoritiespreferences.rst:584
msgid "Creates a link to the last authority record that matches a given heading, even if there is more than one record that matches"
msgstr ""

#: ../../source/authoritiespreferences.rst:589
msgid "This preference tells Koha which match to use when searching for authority matches when saving a record."
msgstr ""

#: ../../source/authoritiespreferences.rst:595
msgid "LinkerOptions"
msgstr ""

#: ../../source/authoritiespreferences.rst:597
msgid "Asks: Set the following options for the authority linker \\_\\_\\_"
msgstr ""

#: ../../source/authoritiespreferences.rst:601
msgid "This feature is experimental and shouldn't be used in a production environment until further expanded upon."
msgstr ""

#: ../../source/authoritiespreferences.rst:606
msgid "This is a pipe separated (\\|) list of options. At the moment, the only option available is \"broader\\_headings.\" With this option set to \"broader\\_headings\", the linker will try to match the following heading as follows:"
msgstr ""

#: ../../source/authoritiespreferences.rst:615
msgid "First: Camins-Esakov, Jared--Coin collections--Catalogs--Early works to 1800"
msgstr ""

#: ../../source/authoritiespreferences.rst:618
msgid "Next: Camins-Esakov, Jared--Coin collections--Catalogs"
msgstr ""

#: ../../source/authoritiespreferences.rst:620
msgid "Next: Camins-Esakov, Jared--Coin collections"
msgstr ""

#: ../../source/authoritiespreferences.rst:622
msgid "Next: Camins-Esakov, Jared (matches! if a previous attempt had matched, it would not have tried this)"
msgstr ""

#: ../../source/authoritiespreferences.rst:628
msgid "LinkerRelink"
msgstr ""

#: ../../source/authoritiespreferences.rst:630
msgid "Default: Do"
msgstr ""

#: ../../source/authoritiespreferences.rst:632
msgid "Asks: \\_\\_\\_ relink headings that have previously been linked to authority records."
msgstr ""

#: ../../source/authoritiespreferences.rst:643
msgid "When set to 'Do', the linker will confirm the links for headings that have previously been linked to an authority record when it runs, correcting any incorrect links it may find. When set to 'Do not', any heading with an existing link will be ignored, even if the existing link is invalid or incorrect."
msgstr ""

#: ../../source/images.rst:779
msgid "In the authority record detail page in the staff interface, the broader and narrower terms are displayed in a tree"
msgstr ""
