# Spanish translations for Koha Manual package.
# Copyright (C) 2018, Koha Community
# This file is distributed under the same license as the Koha Manual package.
# Automatically generated, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: Koha Manual 22.11\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2025-02-04 01:00+0000\n"
"PO-Revision-Date: 2020-06-10 04:07+0000\n"
"Last-Translator: Pablo Bianchi <pablo.bianchi@gmail.com>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1591762034.143912\n"
"X-Pootle-Path: /es/manual22.11/webservices.po\n"
"X-Pootle-Revision: 1\n"

#: ../../source/images.rst:4266
msgid "image1336"
msgstr ""

#: ../../source/images.rst:4267
msgid "image1337"
msgstr ""

#: ../../source/images.rst:4268
msgid "image1338"
msgstr ""

#: ../../source/images.rst:4269
msgid "image1339"
msgstr ""

#: ../../source/webservices.rst:6
#, fuzzy
msgid "Web services"
msgstr "Servicios Web"

#: ../../source/webservices.rst:8
msgid "Koha provides a number of APIs allowing access to it's data and functions."
msgstr ""

#: ../../source/webservices.rst:13
msgid "OAI-PMH"
msgstr "OAI-PMH"

#: ../../source/webservices.rst:15
msgid "For the Open Archives Initiative-Protocol for Metadata Harvesting (OAI-PMH) there are two groups of 'participants': Data Providers and Service Providers. Data Providers (open archives, repositories) provide free access to metadata, and may, but do not necessarily, offer free access to full texts or other resources. OAI-PMH provides an easy to implement, low barrier solution for Data Providers. Service Providers use the OAI interfaces of the Data Providers to harvest and store metadata. Note that this means that there are no live search requests to the Data Providers; rather, services are based on the harvested data via OAI-PMH."
msgstr "Para el Open Archives Initiative-Protocol for Metadata Harvesting (OAI-PMH) hay dos grupos de 'participantes': Proveedores de datos y Proveedores de servicios. Los Proveedores de datos (archivos abiertos, repositorios)  proveen acceso libre a metadatos, y muchos, aunque no necesariamente, ofrecen acceso libre a textos completos y otros recursos. OAI-PMH provee una solución fácil de implementar y sencilla para Proveedores de datos. Los Proveedores de servicios utilizan la interfaz OAI de los Proveedores de datos para cosechar y almacenar metadatos. Note que esto significa que no hay pedidos de búsqueda directas a los Proveedores de datos; mas bien los servicios se basan en los datos cosechados via OAI-PMH."

# Web services > OAI-PMH
#: ../../source/webservices.rst:26
msgid "Learn more about OAI-PMH at: http://www.openarchives.org/pmh/"
msgstr "Para aprender más sobre OAI-PMH diríjase a: https://www.openarchives.org/pmh/"

#: ../../source/webservices.rst:28
msgid "Koha at present can only act as a Data Provider. It can not harvest from other repositories. The biggest stumbling block to having Koha harvest from other repositories is that MARC is the only metadata format that Koha indexes natively."
msgstr ""

#: ../../source/webservices.rst:33
msgid "To enable OAI-PMH in Koha edit the :ref:`OAI-PMH <oai-pmh-pref-label>` preference. Once enabled you can visit http://YOURKOHACATALOG/cgi-bin/koha/oai.pl to see your file."
msgstr ""

#: ../../source/webservices.rst:37
msgid "By default Koha won't include item information in OAI-PMH result sets, but they can be added by using the include_items option in the a configuration file linked from :ref:`OAI-PMH:ConfFile <oai-pmh-conffile-label>`."
msgstr ""

#: ../../source/webservices.rst:41
msgid "Note that the sample conf file below contains both marc21 and marcxml, because marc21 is the metadata prefix recommended by the `OAI-PMH guidelines <https://www.openarchives.org/OAI/2.0/guidelines-marcxml.htm>`_ while marcxml was the only one in the sample before Koha 23.11 (and support for marc21 was added in Koha 17.05)."
msgstr ""

#: ../../source/webservices.rst:50
#, fuzzy
msgid "Sample OAI conf file"
msgstr "Muestra de archivo de configuración OAI"

#: ../../source/webservices.rst:76
msgid "The options are:"
msgstr ""

#: ../../source/webservices.rst:78
msgid "xsl\\_file: Path to an XSLT file that will be used for transforming the Koha MARCXML data into the needed structure/format. It can be useful, for example, if you need some specific Dublin Core fields to be generated instead of just the default ones."
msgstr ""

#: ../../source/webservices.rst:83
msgid "include\\_items: If set to 1, item information will be included in the response according to the MARC framework's mapping."
msgstr ""

#: ../../source/webservices.rst:86
msgid "expanded\\_avs: If set to 1, all coded values will the expanded with descriptions. This includes library names, item type descriptions, authorized value descriptions and classification source descriptions."
msgstr ""

#: ../../source/webservices.rst:90
msgid "All these options can be used with different metadataPrefix entries, allowing the consumers to request one or the other."
msgstr ""

#: ../../source/webservices.rst:96
#, fuzzy
msgid "SRU server"
msgstr "Nuevo servidor SRU"

#: ../../source/webservices.rst:98
msgid "Koha implements the Search/Retrieve via URL (SRU) protocol. More information about the protocol itself can be found at http://www.loc.gov/standards/sru/. The version implemented is version 1.1."
msgstr ""

#: ../../source/webservices.rst:106
msgid "Explain"
msgstr "Explain"

#: ../../source/webservices.rst:108
msgid "If you want to have information about the implementation of SRU on a given server, you should have access to the Explain file using a request to the server without any parameter. Like http://myserver.com:9999/biblios/. The response from the server is an XML file that should look like the following and will give you information about the default settings of the SRU server."
msgstr ""

#: ../../source/webservices.rst:176
msgid "Search"
msgstr "Buscar"

#: ../../source/webservices.rst:178
msgid "This url : http://myserver.com:9999/biblios?version=1.1&operation=searchRetrieve&query=reefs is composed of the following elements:"
msgstr ""

#: ../../source/webservices.rst:182
msgid "base url of the SRU server : http://myserver.com:9999/biblios?"
msgstr "URL base del servidor SRU: http://myserver.com:9999/biblios?"

#: ../../source/webservices.rst:184
msgid "search part with the 3 required parameters : version, operation and query. The parameters within the search part should be of the key=value form, and can be combined with the & character."
msgstr ""

#: ../../source/webservices.rst:188
msgid "One can add optional parameters to the query, for instance maximumRecords indicating the maximum number of records to be returned by the server. So http://myserver.com:9999/biblios?version=1.1&operation=searchRetrieve&query=reefs&maximumRecords=5 will only get the first 5 results results from the server."
msgstr ""

#: ../../source/webservices.rst:194
msgid "The \"operation\" key can take two values: scan or searchRetrieve."
msgstr "La clave \"operation\" puede tomar dos valores: scan o searchRetrieve."

#: ../../source/webservices.rst:196
msgid "If operation=searchRetrieve, then the search key should be query. As in : operation=searchRetrieve&query=reefs"
msgstr ""

#: ../../source/webservices.rst:199
msgid "If operation=scan, then the search key should be scanClause. As in : operation=scan&scanClause=reefs"
msgstr ""

#: ../../source/webservices.rst:202
msgid "etc/zebradb/biblios/etc/bib1.att defines Zebra/3950 indexes that exist on your system. For instance you'll see that we have indexes for Subject and for Title: att 21 Subject and att 4 Title respectively."
msgstr "etc/zebradb/biblios/etc/bib1.att define los índices Zebra/3950 que existen en el sistema. Por ejemplo usted verá que tenemos índices por Materia y por Título: att 21 Subject y att Title 4, respectivamente."

#: ../../source/webservices.rst:206
msgid "In the pqf.properties file located under etc/zebradb/pqf.properties I see that an access point already uses my Subject index (index.dc.subject = 1=21) while another uses my Title index (index.dc.title = 1=4) I know this is my Subject index because as I've seen just before in my bib1.att file, it's called with =1=21 in Z3950: so index.dc.subject = 1=21 correctly points to my Subject index. And Title was called with 1=4 so index.dc.title = 1=4 correctly points to my Title index. I can now construct my query just like I would in a search box, just preceding it with the \"query\" key: query=Subject=reefs and Title=coral searches \"reefs\" in the subject and \"coral\" in the title. The full url would be http://myserver.com:9999/biblios?version=1.1&operation=searchRetrieve&query=Subject=reefs and Title=coral If I want to limit the result set to just 5 records, I can do http://myserver.com:9999/biblios?version=1.1&operation=searchRetrieve&query=Subject=reefs and Title=coral&maximumRecords=5"
msgstr ""

#: ../../source/webservices.rst:222
msgid "I can also play with truncate, relations, etc. Those are also defined in my pqf.properties file. I can see for instance the position properties defined as:"
msgstr "También puedo hacerlo con truncamiento, relaciones, etc. Éstas se definen también en el archivo pqf.properties. Se puede ver por ejemplo las propiedades de posición definidas como:"

#: ../../source/webservices.rst:233
msgid "So as an example if I want \"coral\" to be at the beginning of the title, I can do this query : http://myserver.com:9999/biblios?version=1.1&operation=searchRetrieve&query=Title=coral first"
msgstr ""

#: ../../source/webservices.rst:241
msgid "Retrieve"
msgstr "Retrieve"

#: ../../source/webservices.rst:243
msgid "My search for http://univ\\_lyon3.biblibre.com:9999/biblios?version=1.1&operation=searchRetrieve&query=coral reefs&maximumRecords=1 retrieves just on record. The response looks like this:"
msgstr ""

#: ../../source/webservices.rst:314
msgid "ILS-DI"
msgstr "ILS-DI"

#: ../../source/webservices.rst:316
msgid "As of writing, the self documenting ILS-DI is the most complete interface. After it has been enabled as described in the :ref:`ILS-DI system preferences <webservices-ils-di-system-preferences-label>` section, the documentation should be available at https://YOURKOHACATALOG/cgi-bin/koha/ilsdi.pl"
msgstr ""

#: ../../source/webservices.rst:324
#, fuzzy
msgid "JSON reports services"
msgstr "Servicios Web"

#: ../../source/webservices.rst:326
#, fuzzy
msgid "Koha implements a JSON reports service for every report saved using the :ref:`Guided reports wizard <guided-report-wizard-label>` or :ref:`Report from SQL <report-from-sql-label>` features."
msgstr "Los datos en Koha se almacenan en una base de datos MySQL lo cual significa que los bibliotecarios pueden generar casi cualquier informe que deseen ya sea usando el `Asistente de informes guiados <guided-report-wizard-label>` o confeccionando su propia `consulta SQL <report-from-sql-label>`."

#: ../../source/webservices.rst:330
msgid "By default reports will be non-public and only accessible by authenticated users. If a report is explicitly set to *public* it will be accessible without authentication by anyone. This feature should only be used when the data can be shared safely not containing any patron information."
msgstr ""

#: ../../source/webservices.rst:335
#, fuzzy
msgid "The reports can be accessed using the following URLs:"
msgstr "Los indicadores se pueden acceder utilizando la llave 'indicator'."

#: ../../source/webservices.rst:337
#, fuzzy
msgid "Public reports"
msgstr "Notas públicas"

#: ../../source/webservices.rst:339
msgid "OpacBaseURL/cgi-bin/koha/svc/report?id=REPORTID"
msgstr "OpacBaseURL/cgi-bin/koha/svc/report?id=REPORTID"

#: ../../source/webservices.rst:341
#, fuzzy
msgid "Non-public reports"
msgstr "Notas no públicas"

#: ../../source/webservices.rst:343
msgid "StaffBaseURL/cgi-bin/koha/svc/report?id=REPORTID"
msgstr "StaffBaseURL/cgi-bin/koha/svc/report?id=REPORTID"

#: ../../source/webservices.rst:345
msgid "There are also some additional parameters available:"
msgstr "También hay algunos parámetros adicionales disponibles:"

#: ../../source/webservices.rst:347
msgid "Instead of accessing the report by REPORTID you can also use the report's name:"
msgstr ""

#: ../../source/webservices.rst:350
msgid ".../cgi-bin/koha/svc/report?name=REPORTNAME"
msgstr ".../cgi-bin/koha/svc/report?name=REPORTNAME"

#: ../../source/webservices.rst:352
msgid "For easier development there is also an option to generate an annotated output of the data. It will generate an array of hashes that include the field names as keys."
msgstr ""

#: ../../source/webservices.rst:356
msgid ".../cgi-bin/koha/svc/report?name=REPORTNAME&annotated=1"
msgstr ".../cgi-bin/koha/svc/report?name=REPORTNAME&annotated=1"

#: ../../source/webservices.rst:361
msgid "Versioned RESTful API effort"
msgstr ""

#: ../../source/webservices.rst:363
msgid "There is an ongoing effort to converge the APIs above into a single versioned set of modern RESTful endpoints documented using the OpenAPI standard and available by default under https://YOURKOHACATALOG/api/v1/"
msgstr ""

#: ../../source/webservices.rst:367
msgid "Full documentation for these APIs for your version of Koha can be found at `api.koha-community.org <https://api.koha-community.org>`_."
msgstr ""

#: ../../source/webservices.rst:373
msgid "OAuth2 client credentials grant"
msgstr ""

#: ../../source/webservices.rst:375
msgid "Koha supports the OAuth2 client credentials grant as a means to secure the API for using it from other systems to adhere to current industry standards. More information on the OAuth2 client credentials grant standard `can be found here <https://auth0.com/docs/api-auth/grant/client-credentials>`_."
msgstr ""

#: ../../source/webservices.rst:381
msgid "API key management interface for patrons"
msgstr ""

#: ../../source/webservices.rst:383
msgid "In order for API keys to be create for patrons, the system preference :ref:`RESTOAuth2ClientCredentials <restoauth2clientcredentials-label>` **must** be enabled for the option to appear in a patron record."
msgstr ""

#: ../../source/webservices.rst:387
msgid "Navigate to a patron record and select *More > Manage API keys*"
msgstr ""

#: ../../source/webservices.rst:389
msgid "|image1336|"
msgstr ""

#: ../../source/webservices.rst:391
msgid "If no API keys exist for a patron there will be a message prompting to generate a client id/secret pair"
msgstr ""

#: ../../source/webservices.rst:394
msgid "|image1337|"
msgstr ""

#: ../../source/webservices.rst:396
#, fuzzy
msgid "Enter a description for the client id/secret pair and click Save"
msgstr "Generar un nuevo par id de cliente/secreto"

#: ../../source/webservices.rst:398
msgid "|image1338|"
msgstr ""

#: ../../source/webservices.rst:400
msgid "Koha will generate a client id/secret pair for use to connect to Koha from other third-party systems as an authenticated client"
msgstr ""

#: ../../source/webservices.rst:403
msgid "|image1339|"
msgstr ""

#: ../../source/webservices.rst:405
msgid "Clicking the Revoke button next to an API credential pair will render the specific credential pair inactive until reactivated"
msgstr ""

#: ../../source/webservices.rst:411
msgid "Barcode image generator"
msgstr ""

#: ../../source/webservices.rst:413
msgid "Koha provides a barcode image generator on both the staff interface and the public interface. Both require a user to be logged in to use the service, to prevent abuse by third parties."
msgstr ""

#: ../../source/webservices.rst:416
msgid "/cgi-bin/koha/svc/barcode?barcode=123456789&type=UPCE"
msgstr ""

#: ../../source/webservices.rst:416 ../../source/webservices.rst:438
msgid "For example::"
msgstr ""

#: ../../source/webservices.rst:418
msgid "The URL above will generate a barcode image for the barcode \"123456789\", using the UPCE barcode format."
msgstr ""

#: ../../source/webservices.rst:420
msgid "The available barcode types are: * Code39 * UPCE * UPCA * QRcode * NW7 * Matrix2of5 * ITF * Industrial2of5 * IATA2of5 * EAN8 * EAN13 * COOP2of5"
msgstr ""

#: ../../source/webservices.rst:434
msgid "If no type is specified, Code39 will be used."
msgstr ""

#: ../../source/webservices.rst:436
msgid "By default, the barcode image will contain the text of the barcode as well. If this is not desired, the parameter \"notext\" may be passed to suppress this behavior."
msgstr ""

#: ../../source/webservices.rst:442
msgid "will generate a barcode image 123456789 without the text \"123456789\" below it."
msgstr ""

#: ../../source/webservices.rst:444
msgid "This service can be used to embed barcode images into slips and notices printed from the browser, and to embed a patron cardnumber into the OPAC, among other possibilities."
msgstr ""
